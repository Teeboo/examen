
# Projet web bacinfo2 
Maeck Thibault.

Dynamic school website bound with mysql database.


## Minimal requirements
- PHP 8.1.10+
- MySQL 5.7.31+

## Installation

- Have this project in your `wamp64/www`

- Copy `config-dist.php` to the same directory, rename it by `config.php`

- Create your mysql database and `match` her `parameters` with the one in `config.php`


## Migration

Execute this command with `Git Bash` from `wamp64/www/examen/migration`

```bash
  php migration.php AllInOne
```


## Functioning

An admin user is by default created with the migration.
For using it, log in with :

- `Email` = `Admin@hotmail.com`

- `Password` = `A`

Admin can access `user`, `student` and `teacher` list on the admin dropdown menu from navbar and perform several action on user of these list

`Inscription from student` are also availaible from this menu, where you can accept or refuse `request` from user to become `student` in a `formation`

This project come with 2 base formations linked with theirs courses.

Admin have the possibility to manage formation from 2 menu :

-   Formation(Admin) in navbar, where you can `manage formation`, set them `active` and `create` your own custom `formation`, and `changing` the `current academic year`.

-   `Cursus scolaire` from cours navbar, where you can `set inactive` a `formation`, `add Teacher` linked to that formation from a list of user.

By default, when you create your account , you will be given the role `guest`. From that role , you can `register` in a `formation` from `Cours->Cursus scolaire` in the navbar dropdown menu.

This `registration` has to be `accepted` by a `admin`

Once it's done, you will get `access` to `more dropdown menu` from `Cours` in `navbar`

-   `Inscription cours` : You can from there access to all `availaible course` from the `formation` you're `register` on in the current year.

-   `Mes cours` where you can `access` all `course` that you're `register on` and check if the course is finished and the `note` you get from that course. All `finished course` will still `appear` in this section.

Once you're done with all the course from the first branche of your formation , admin can register you to the next year branche.

`Teacher` have `dropdown menu` from navbar called `teacher`. From there, they can access two pages : 

-   Inscription cours : Teacher can register from there to all availaible course from their formations

-   Mes cours : Teacher can manage their courses, reject them to let another professor register , end courses if they are finished, and change note from student register.
## Bugs

Some bugs are still in this project and will be fix soon.

There is a possibility that you encounter some of them : 

-   Some Modal could possibly not open correctly from `Teacher -> Mes cours -> Student(button)` and `Admin -> List - Student -> Note (button)`.


- Some text and title from Formation and course name appear with "?" instead of special character, it come from `ASCII conversion` and can be fix by rewriting varchar data send in mysql database by hand and not with copy paste.