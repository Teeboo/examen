<?php

namespace app\Models;

class course extends Model
{
    /**
     *
     */
    public function __construct() {
        parent::__construct();
    }


    /**
     * @param string $userid
     * @return mixed
     */
    public static function checkStudentRole(string $userid): mixed{

        $checking = self::$connect->prepare("SELECT COUNT(*) FROM t_student WHERE userid = ? AND accepted = 1 AND removed = 0");
        $checking->execute([$userid]);
        return $checking->fetchColumn();
    }

    /**
     * @param string $userid
     * @return mixed
     */
    public static function checkTeacherRole(string $userid): mixed{

        $checking = self::$connect->prepare("SELECT COUNT(*) FROM t_teacher WHERE userid = ? AND rejected = 0");
        $checking->execute([$userid]);
        return $checking->fetchColumn();
    }

    /**
     * @return array
     */
    public static function getFormationList(): array
    {
        $formationList = [];
        $result = self::$connect->prepare("SELECT * FROM formation where status != 0");
        $result->execute();
        while ($data_tmp = $result->fetchObject()) {
            $formationList[] = $data_tmp;
        }
        return $formationList;
    }

    /**
     * @return array
     */
    public static function getAdminRemovedFormationList(): array
    {
        $formationList = [];
        $result = self::$connect->prepare("SELECT formationTitle,Level FROM formation where status != 1");
        $result->execute();
        while ($data_tmp = $result->fetchObject()) {
            $formationList[] = $data_tmp;
        }
        return $formationList;
    }

    /**
     * @param string $FormationTitle
     * @return string
     */
    public static function getFirstBrancheFromFormation(string $FormationTitle): string
    {
        $result = self::$connect->prepare("SELECT nom FROM branche as b inner join formation as f  on b.formationTitle = f.formationTitle where f.formationTitle = ? order by b.orderInt limit 1;");
        $result->execute([$FormationTitle]);
        return $result->fetchColumn();
    }

    /**
     * @param string $userid
     * @param string $BrancheName
     * @return mixed
     */
    public static function CheckIfStudentAlreadyRegister(string $userid , string $BrancheName): mixed{

        $checking = self::$connect->prepare("SELECT COUNT(*) FROM t_student WHERE userid = ? AND fk_brancheName = ?");
        $checking->execute([$userid,$BrancheName]);
        return $checking->fetchColumn();
    }

    /**
     * @param string $userid
     * @param int $accepted
     * @return array
     */
    public static function GetStudentAwaitingOrAcceptedApplication(string $userid, int $accepted =0): array
    {
        $usersList = [];
        $result = self::$connect->prepare("SELECT b.formationTitle 
                                                    FROM t_student as t inner join branche as b on t.fk_brancheName = b.nom inner join formation as f on b.formationTitle = f.formationTitle 
                                                        WHERE t.userid = ? AND t.accepted = ?");
        $result->execute([$userid,$accepted]);
        while ($data_tmp = $result->fetchObject()) {
            $usersList[] = $data_tmp;
        }
        return $usersList;
    }

    /**
     * @param string $userid
     * @return array
     */
    public static function GetStudentRejectedApplication(string $userid): array
    {
        $usersList = [];
        $result = self::$connect->prepare("SELECT b.formationTitle 
                                                    FROM t_student as t inner join branche as b on t.fk_brancheName = b.nom inner join formation as f on b.formationTitle = f.formationTitle 
                                                        WHERE t.userid = ? AND t.rejected = 1");
        $result->execute([$userid]);
        while ($data_tmp = $result->fetchObject()) {
            $usersList[] = $data_tmp;
        }
        return $usersList;
    }

    /**
     * @param string $userid
     * @param string $BrancheName
     * @return void
     */
    public static function FormationApplicationFromUser(string $userid , string $BrancheName): void{

            $result = self::$connect->prepare("INSERT INTO t_student (userid,fk_brancheName) VALUES (?,?)");
            $result->execute([$userid,$BrancheName]);
    }

    /**
     * @param string $FormationTitle
     * @return array
     */
    public static function GetCourseListByFormation(string $FormationTitle): array
    {
        $CourseList = [];
        $result = self::$connect->prepare("select c.nom,b.nom as brancheName 
                                                    from course as c inner join branche as b on c.FK_branchename = b.nom inner join formation as f on b.formationTitle = f.formationTitle 
                                                        where f.formationtitle = ?"
                                          );
        $result->execute([$FormationTitle]);

        while ($data_tmp = $result->fetchObject()) {
            $CourseList[] = $data_tmp;
        }
        return $CourseList;
    }

    /**
     * @return array
     */
    public static function getAwaitingStudentList(): array
    {
        $StudentList = [];
        $result = self::$connect->prepare("select  u.id ,u.lastname , u.firstname , u.username , b.nom,f.formationTitle 
                                                    FROM user as u inner join t_student as t on u.id = t.userid inner join branche as b on t.fk_branchename = b.nom inner join formation as f on b.formationTitle = f.formationTitle 
                                                        WHERE t.accepted = 0 AND t.rejected = 0;");
        $result->execute();
        while ($data_tmp = $result->fetchObject()) {
            $StudentList[] = $data_tmp;
        }
        return $StudentList;
    }

    /**
     * @param string $userid
     * @param string $BrancheName
     * @return void
     */
    public static function AcceptStudent(string $userid , string $BrancheName): void{

        $result = self::$connect->prepare("UPDATE t_student SET accepted = 1 where userid = ? AND fk_brancheName = ?");
        $result->execute([$userid,$BrancheName]);
    }

    /**
     * @param string $userid
     * @param string $BrancheName
     * @return void
     */
    public static function RefuseStudent(string $userid , string $BrancheName): void{

        $result = self::$connect->prepare("UPDATE  t_student SET rejected  = 1 WHERE userid = ? AND fk_brancheName = ?");
        $result->execute([$userid,$BrancheName]);
    }

    /**
     * @param string $userid
     * @param string $Courseid
     * @return void
     */
    public static function AcceptCourse(string $userid , string $Courseid): void{

        $result = self::$connect->prepare("INSERT INTO student_course (fk_userid,fk_yearly_courseID) VALUES (?,?)");
        $result->execute([$userid,$Courseid]);
    }

    /**
     * @param string $userid
     * @param string $Courseid
     * @return int
     */
    public static function CheckCourse(string $userid , string $Courseid): int{

        $checking = self::$connect->prepare("SELECT COUNT(*) FROM student_course WHERE fk_userid = ? AND fk_yearly_courseID = ?");
        $checking->execute([$userid,$Courseid]);
        return $checking->fetchColumn();
    }

    /**
     * @param string $coursename
     * @param int $year
     * @return string
     */
    public static function getIdFromCourse(string $coursename, int $year): string {

        $result = self::$connect->prepare("SELECT id from yearly_course where fk_courseName = ? AND year = ?");
        $result->execute([$coursename,$year]);
        return $result->fetchColumn();

    }

    /**
     * @return string
     */
    public static function getCurrentYear(): string {
        $result = self::$connect->prepare("SELECT year FROM year where current = 1;");
        $result->execute();
        return $result->fetchColumn();
    }

    /**
     * @param string $userid
     * @param string $year
     * @return array
     *  return as array Course where :
     *  1)  In branche where Student is accepted
     *  2)  where formation is still active
     *  3)  where year is the actual one
     *  4)  where student haven't finished it with 10+ note
     */
    public static function AvailaibleCourseList(string $userid, string $year) : array {

            $CourseList = [];
            $result = self::$connect->prepare("
                SELECT fk_courseName,FK_branchename,year
                    FROM yearly_course as y inner join course c on y.fk_courseName = c.nom
                        WHERE year = ?
                        AND FK_branchename IN
                        (
                            SELECT fk_branchename
                                FROM t_student
                                    where userid = ? AND accepted = 1
                        )
                        AND FK_branchename NOT IN 
                          (
                            SELECT nom
                                FROM branche as b inner join formation f on b.formationTitle = f.formationTitle
                                    WHERE f.status=0
                          
                          )
                        AND fk_courseName NOT IN
                        (
                                      SELECT fk_courseName
                                        FROM yearly_course as y inner join course c on y.fk_courseName = c.nom inner join student_course sc on y.id = sc.fk_yearly_courseID
                                            where fk_userid = ? AND finished = 0
                        )
                        AND fk_courseName NOT IN
                        (
                                SELECT fk_courseName
                                    FROM yearly_course as y inner join course c on y.fk_courseName = c.nom inner join student_course sc on y.id = sc.fk_yearly_courseID
                                        where fk_userid = ? AND finished = 1 AND note > 9
                        )
                        AND y.id NOT IN
                        (
                            
                                SELECT fk_yearly_courseID
                                    FROM student_course
                                        WHERE finished = 1
                                        AND
                                        fk_userid = ?            
                        )

            ");
            $result->execute([$year,$userid,$userid,$userid,$userid]);

            while ($data_tmp = $result->fetchObject()) {
                $CourseList[] = $data_tmp;
            }
            return $CourseList;
    }

    /**
     * @param string $userid
     * @param string $year
     * @return array
     */
    public static function TeacherAvailableCourseList(string $userid, string $year) : array{

        $CourseList = [];
        $result = self::$connect->prepare("
                                                SELECT id,b.nom,fk_courseName, year
                                                    FROM yearly_course as yc inner join course c on yc.fk_courseName = c.nom inner join branche b on c.FK_branchename = b.nom
                                                        WHERE teacher IS NULL
                                                        AND year = ?
                                                        AND b.formationTitle IN (    SELECT f.formationTitle
                                                                                        FROM formation as f inner join branche b on f.formationTitle = b.formationTitle inner join t_teacher tt on b.nom = tt.fk_brancheName
                                                                                            WHERE userid = ?
                                                                                            AND
                                                                                            tt.rejected = 0
                                                                                        GROUP BY f.formationTitle)
                                                        AND status = 0;
                                          ");
        $result->execute([$year,$userid]);

        while ($data_tmp = $result->fetchObject()) {
            $CourseList[] = $data_tmp;
        }
        return $CourseList;
    }

    /**
     * @param string $courseid
     * @return mixed
     */
    public static function CheckCourseTeacher(string $courseid) : mixed{

        $checking = self::$connect->prepare("SELECT COUNT(*) FROM yearly_course WHERE id = ? AND teacher IS NULL");
        $checking->execute([$courseid]);
        return $checking->fetchColumn();

    }

    /**
     * @param string $userid
     * @param string $courseid
     * @return void
     */
    public static function InsertTeacherInCourse(string $userid, string $courseid): void{
        $result = self::$connect->prepare("INSERT INTO teacher_course (fk_userid,fk_yearly_courseID) VALUES (?,?)");
        $result->execute([$userid,$courseid]);

        $update = self::$connect->prepare("UPDATE yearly_course SET teacher = ? WHERE id = ?");
        $update->execute([$userid,$courseid]);
    }

    /**
     * @param string $userid
     * @param string $courseid
     * @return mixed
     */
    public static function CHECKInsertTeacherInCourse(string $userid, string $courseid): mixed{
        $checking = self::$connect->prepare("SELECT COUNT(*) FROM teacher_course WHERE fk_userid = ? AND fk_yearly_courseID = ?");
        $checking->execute([$userid,$courseid]);
        return $checking->fetchColumn();
    }

    /**
     * @param string $userid
     * @param string $courseid
     * @return mixed
     */
    public static function CHECKInsertTeacherInYearlyCourse(string $userid, string $courseid): mixed{
        $checking = self::$connect->prepare("SELECT COUNT(*) FROM yearly_course WHERE teacher = ? AND id = ?");
        $checking->execute([$userid,$courseid]);
        return $checking->fetchColumn();
    }

    /**
     * @param string $formationTitle
     * @return mixed
     */
    public static function CHECKIfFormationTitleAvailaible(string $formationTitle): mixed{
        $checking = self::$connect->prepare("SELECT COUNT(*) FROM formation WHERE formationTitle = ?");
        $checking->execute([$formationTitle]);
        return $checking->fetchColumn();
    }

    /**
     * @param string $userid
     * @return array
     */
    public static function CurrentCourseList(string $userid) : array {

        $CourseList = [];
        $result = self::$connect->prepare("SELECT fk_courseName,FK_branchename,year,note,finished
                                                    FROM yearly_course as y inner join course c on y.fk_courseName = c.nom inner join student_course sc on y.id = sc.fk_yearly_courseID
                                                        where fk_userid = ?

                                          ");
        $result->execute([$userid]);

        while ($data_tmp = $result->fetchObject()) {
            $CourseList[] = $data_tmp;
        }
        return $CourseList;

    }

    /**
     * @param string $formationTitle
     * @return array
     */
    public static function PotentialTeacherList(string $formationTitle) : array {

        $TeacherList = [];
        $result = self::$connect->prepare("    SELECT id,username,lastname,firstname
                                                        FROM user
                                                            where id NOT IN ( SELECT userid
                                                                                FROM t_student inner join branche b on t_student.fk_brancheName = b.nom
                                                                                    WHERE formationTitle = ? AND accepted = 1)
                                                                        AND id NOT IN( SELECT userid 
                                                                                FROM t_teacher inner join branche b on t_teacher.fk_brancheName = b.nom
                                                                                    WHERE formationTitle = ?)
                                          ");
        $result->execute([$formationTitle,$formationTitle]);

        while ($data_tmp = $result->fetchObject()) {
            $TeacherList[] = $data_tmp;
        }
        return $TeacherList;

    }

    /**
     * @param string $userid
     * @param string $formationTitle
     * @return mixed
     */
    public static function InsertTeacherInFormation(string $userid , string $formationTitle): mixed{

        $Insert = self::$connect->prepare("    INSERT INTO t_teacher (userid,fk_brancheName) 
                                                            SELECT $userid as userid,nom
                                                                FROM branche
                                                                    WHERE formationTitle = ?");
        $Insert->execute([$formationTitle]);
        return $Insert->fetchColumn();
    }

    /**
     * @param string $userid
     * @param string $formationTitle
     * @return mixed
     */
    public static function CheckInsertTeacherInFormation(string $userid , string $formationTitle): mixed{

        $Insert = self::$connect->prepare("SELECT count(*) 
                                                    FROM t_teacher inner join branche b on t_teacher.fk_brancheName = b.nom
                                                        WHERE userid = ? AND formationTitle = ?");
        $Insert->execute([$userid,$formationTitle]);

        return $Insert->fetchColumn();
    }

    /**
     * @param string $teacherid
     * @param int $year
     * @return array
     */
    public static function getTeacherCourseList(string $teacherid, int $year): array
    {
        $CourseList = [];
        $result = self::$connect->prepare("    SELECT id,fk_courseName,year,c.FK_branchename
                                                        FROM yearly_course inner join course c on yearly_course.fk_courseName = c.nom
                                                            WHERE teacher = ?                                           
                                                            AND
                                                                status = 0"
                                         );
        $result->execute([$teacherid]);
        while ($data_tmp = $result->fetchObject()) {
            $CourseList[] = $data_tmp;
        }
        return $CourseList;
    }

    /**
     * @param string $courseId
     * @return array
     */
    public static function getAllStudentFromCourseId(string $courseId): array
    {
        $studentList = [];
        $result = self::$connect->prepare("SELECT student_course.fk_yearly_courseID,user.id,lastname , firstname,note
                                                    FROM user inner join student_course on user.id = student_course.fk_userid 
                                                        WHERE fk_yearly_courseID = ?                                                 
                                         ");
        $result->execute([$courseId]);
        while ($data_tmp = $result->fetchObject()) {
            $studentList[] = $data_tmp;
        }
        return $studentList;
    }

    /**
     * @param string $userid
     * @param string $courseId
     * @param string $note
     * @return bool
     */
    public static function UpdateStudentNote(string $userid , string $courseId, string $note): bool{

        $update = self::$connect->prepare("UPDATE student_course SET note = ? WHERE fk_userid = ? AND fk_yearly_courseID = ?");
        $update->execute([$note,$userid,$courseId]);

        $select = self::$connect->prepare("SELECT note FROM student_course WHERE fk_userid = ? AND fk_yearly_courseID = ?");
        $select->execute([$userid,$courseId]);
        if($select == $note){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * @param string $courseId
     * @return mixed
     */
    public static function EndCourse(string $courseId): mixed{

        $update = self::$connect->prepare("UPDATE yearly_course SET status = 1 WHERE id = ?");
        $update->execute([$courseId]);

        $update2 = self::$connect->prepare("UPDATE student_course SET finished = 1 WHERE fk_yearly_courseID = ?");
        $update2->execute([$courseId]);

        $select = self::$connect->prepare("SELECT COUNT(finished) FROM student_course WHERE fk_yearly_courseID = ? AND finished = 0");
        $select->execute([$courseId]);
        return $select->fetchColumn();
    }

    /**
     * @param string $courseId
     * @param string $userid
     * @return void
     */
    public static function TeacherRejectCourse(string $courseId, string $userid): void{

        $update = self::$connect->prepare("UPDATE yearly_course SET teacher = null WHERE id = ?");
        $update->execute([$courseId]);

        $update2 = self::$connect->prepare("DELETE FROM teacher_course WHERE fk_userid = ? AND fk_yearly_courseID = ?");
        $update2->execute([$userid,$courseId]);

    }

    /**
     * @return array
     */
    public static function TeacherListForAdmin():array{

        $TeacherList = [];
        $result = self::$connect->prepare("    SELECT u.id , lastname, firstname,formationTitle,rejected
                                                        FROM user as u inner join t_teacher tt on u.id = tt.userid inner join branche b on tt.fk_brancheName = b.nom
                                                            
                                                            GROUP BY u.id,formationTitle"
                                         );
        $result->execute([]);
        while ($data_tmp = $result->fetchObject()) {
            $TeacherList[] = $data_tmp;
        }
        return $TeacherList;
    }

    /**
     * @return array
     */
    public static function StudentListForAdmin():array{
        $StudentList = [];
        $result = self::$connect->prepare("    SELECT u.id,b.nom,b.formationTitle ,'validate' as validate,'note' as note, lastname , firstname,removed
                                                        FROM user as u inner join t_student ts on u.id = ts.userid inner join branche b on ts.fk_brancheName = b.nom
                                                            WHERE finished = 0"
        );
        $result->execute([]);
        while ($data_tmp = $result->fetchObject()) {
            $StudentList[] = $data_tmp;
        }
        return $StudentList;

    }

    /**
     * @param string $userid
     * @param string $branchename
     * @return array
     */
    public static function StudentListForAdmin_CourseModal(string $userid, string $branchename):array{

        $StudentCourse = [];
        $result = self::$connect->prepare("    SELECT fk_courseName,year,note,sc.finished
                                                        FROM student_course as sc inner join yearly_course yc on sc.fk_yearly_courseID = yc.id inner join t_student ts on sc.fk_userid = ts.userid
                                                            WHERE userid = ?
                                                            AND
                                                            fk_courseName IN (    SELECT c.nom
                                                                                    FROM course as c inner join branche b on c.FK_branchename = b.nom
                                                                                        WHERE b.nom = ?)
            GROUP BY fk_courseName"
        );

        $result->execute([$userid,$branchename]);
        while ($data_tmp = $result->fetchObject()) {
            $StudentCourse[] = $data_tmp;
        }
        return $StudentCourse;




    }

    /**
     * @param string $userid
     * @param string $formationTitle
     * @return void
     */
    public static function RemoveTeacher(string $userid, string $formationTitle):void{

        $update = self::$connect->prepare("UPDATE yearly_course
                                                    inner join course c on yearly_course.fk_courseName = c.nom inner join branche b on c.FK_branchename = b.nom
                                                        SET teacher = null
                                                            WHERE teacher = ?
                                                            AND   b.nom IN ( SELECT nom
                                                                             FROM branche
                                                                             WHERE formationTitle = ?)"
                                         );
        $update->execute([$userid,$formationTitle]);

        $delete = self::$connect->prepare("DELETE teacher_course
                                                    FROM teacher_course
                                                        WHERE fk_userid = ?
                                                        AND
                                                        fk_yearly_courseID IN (    SELECT id from yearly_course inner join course c on yearly_course.fk_courseName = c.nom inner join branche b on c.FK_branchename = b.nom
                                                                                   WHERE b.nom IN ( SELECT nom
                                                                                                    FROM branche
                                                                                                    WHERE formationTitle = ?))"
                                          );
        $delete->execute([$userid,$formationTitle]);


        $update2 = self::$connect->prepare("    UPDATE t_teacher
                                                        SET rejected = 1
                                                            WHERE userid = ?
                                                            AND
                                                            fk_brancheName IN (    SELECT nom
                                                                                   FROM branche
                                                                                   WHERE formationTitle = ?)"
                                          );

        $update2->execute([$userid,$formationTitle]);

    }

    /**
     * @param string $userid
     * @param string $formationTitle
     * @return void
     */
    public static function ReAddTeacher(string $userid, string $formationTitle):void{

        $update2 = self::$connect->prepare("    UPDATE t_teacher
                                                        SET rejected = 0
                                                            WHERE userid = ?
                                                            AND
                                                            fk_brancheName IN (    SELECT nom
                                                                                   FROM branche
                                                                                   WHERE formationTitle = ?)"
        );

        $update2->execute([$userid,$formationTitle]);



    }

    /**
     * @param string $branchename
     * @return mixed
     */
    public static function GetNextBranche(string $branchename):mixed{

        $NextBranche = self::$connect->prepare("    SELECT b.nom
                                                            FROM branche as b
                                                                WHERE prepreq = ?");
        $NextBranche->execute([$branchename]);

        return $NextBranche->fetchColumn();

    }

    /**
     * @param string $userid
     * @param string $BrancheName
     * @param string $remove
     * @return void
     */
    public static function RemoveStudent(string $userid, string $BrancheName, string $remove):void{

        $update = self::$connect->prepare   ("UPDATE t_student
                                                    SET removed = ?
                                                        WHERE userid = ? AND fk_brancheName = ?
                                            ");
        $update->execute([$remove,$userid,$BrancheName]);

    }

    /**
     * @param string $userid
     * @param string $OldBrancheName
     * @return void
     */
    public static function AdminValidateStudentBranche(string $userid, string $OldBrancheName):void{

        $update = self::$connect->prepare("UPDATE t_student SET finished = 1 WHERE userid = ? AND fk_brancheName = ?");
        $update->execute([$userid,$OldBrancheName]);

    }

    /**
     * @param string $userid
     * @param string $NewBrancheName
     * @return void
     */
    public static function AdminRegisterStudentNewBranche(string $userid, string $NewBrancheName):void{

        $update = self::$connect->prepare("INSERT INTO t_student (userid,fk_brancheName,accepted) VALUES
                                                           (?,?,1)");
        $update->execute([$userid,$NewBrancheName]);
    }

    /**
     * @param string $formationTitle
     * @param string $status
     * @return void
     */
    public static function AdminRemoveFormation(string $formationTitle, string $status):void{

        $update = self::$connect->prepare("UPDATE formation set status = ? WHERE formationTitle = ?");
        $update->execute([$status,$formationTitle]);

    }

    /**
     * @param string $formationTitle
     * @param string $level
     * @param string $time
     * @param string $desc
     * @return void
     */
    public static function AdminAddFormation(string $formationTitle, string $level, string $time, string $desc):void{

        $result = self::$connect->prepare("INSERT INTO formation (formationTitle,Level,`desc`,time,status) VALUES (?,?,?,?,0)");
        $result->execute([$formationTitle,$level,$desc,$time]);

    }

    /**
     * @param string $formationTitle
     * @param string $branchename
     * @param string $order
     * @param string $prepreq
     * @return void
     */
    public static function AdminAddFirstBrancheFromFormation(string $formationTitle, string $branchename, string $order, string $prepreq=''):void{

        if(empty($prepreq)){
            $result = self::$connect->prepare("INSERT INTO branche (formationTitle,nom,orderInt,prepreq) VALUES (?,?,?,NULL)");
            $result->execute([$formationTitle,$branchename,$order]);
        }
        else{
            $result = self::$connect->prepare("INSERT INTO branche (formationTitle,nom,orderInt,prepreq) VALUES (?,?,?,?)");
            $result->execute([$formationTitle,$branchename,$order,$prepreq]);
        }

    }
}