<?php

namespace app\Models;

class year extends Model
{
    public function __construct() {
        parent::__construct();
    }


    /**
     * @return string
     *  return as String the actual year in year table
     */
    public static function getCurrentYear(): string {
        $result = self::$connect->prepare("SELECT year FROM year where current = 1;");
        $result->execute();
        return $result->fetchColumn();
    }

    /**
     * @param string $oldyear
     * @param string $newyear
     * @return void
     */
    public static function ModifyCurrentYear(string $oldyear, string $newyear): void{

        $updateYear = self::$connect->prepare("UPDATE year set current = 0 WHERE year = ?");
        $updateYear->execute([$oldyear]);

        $insertNewYear = self::$connect->prepare("INSERT INTO year () VALUES (?,1)");
        $insertNewYear->execute([$newyear]);
    }

    /**
     * @param string $year
     * @return void
     */
    public static function InsertAllYearlyCourse(string $year):void{

        $insertYearlyCourse = self::$connect->prepare("INSERT INTO yearly_course (fk_courseName,year,status) 
                                                                SELECT nom ,$year as year, 0 as status
                                                                            FROM course
                                                                                WHERE abandoned = 0
                                                      ");
        $insertYearlyCourse->execute();
    }


}