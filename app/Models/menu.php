<?php

namespace app\Models;

class menu extends Model
{
    /**
     *
     */
    public function __construct() {
        parent::__construct();
    }


    /**
     * @param string $languageId
     * @return string
     */
    public static function getLanguageName(string $languageId): string
    {
        $result = self::$connect->prepare("SELECT name FROM language where languageId = ?");
        $result->execute([$languageId]);
        return $result->fetchColumn();
    }

    /**
     * @param string $countryid
     * @return string
     */
    public static function getCountryName(string $countryid): string
    {
        $result = self::$connect->prepare("SELECT countryname FROM country where countryId = ?");
        $result->execute([$countryid]);
        return $result->fetchColumn();
    }







}