<?php

namespace app\Models;
use PDO;
use app\Helpers\Output;

class user extends Model
{

    public function __construct() {
        parent::__construct();
    }

    /**
     * @param array $data
     * @return int
     */
    public static function create(array $data): int
    {

        $insert = self::$connect->prepare(
            "INSERT INTO user (lastname, firstname, address, username, password, email,lang,countryid,phone,newsletter,created) 
                    VALUES 
                    (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,NOW())");
        $insert->execute(array_values($data));
        if ($insert->rowCount()) {
            return self::$connect->LastInsertId();
        }
        return 0;
    }

    /**
     * @param string $username
     * @return int
     */
    public static function checkIfUsernameExist(string $username): int
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user WHERE username = ?");
        $result->execute([$username]);
        return $result->fetchColumn();
    }

    /**
     * @param string $email
     * @return int
     */
    public static function checkIfEmailExist(string $email): int
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user WHERE email = ?");
        $result->execute([$email]);
        return $result->fetchColumn();
    }

    /**
     * @return array
     */
    public static function getUserList(): array
    {
        $usersList = [];
        $result = self::$connect->prepare("SELECT id,image,username,lastname,firstname,email,countryid,created,lastlogin,'role' as role FROM user");
        $result->execute();
        while ($data_tmp = $result->fetchObject()) {
            $usersList[] = $data_tmp;
        }

        return $usersList;
    }

    /**
     * @param string $id
     * @return mixed
     */
    public static function CheckAdmin(string $id): mixed
    {
        $result = self::$connect->prepare("SELECT count(*) FROM user WHERE id = ? AND admin = 1");
        $result->execute([$id]);
        return $result->fetchColumn();

    }

    /**
     * @param string $id
     * @return mixed
     */
    public static function CheckBan(string $id): mixed
    {
        $result = self::$connect->prepare("SELECT count(*) FROM t_ban WHERE userid = ?");
        $result->execute([$id]);
        return $result->fetchColumn();

    }

    /**
     * @param string $id
     * @return void
     */
    public static function Unban(string $id): void{
        $result = self::$connect->prepare("DELETE FROM t_ban where userid = ?");
        $result->execute([$id]);
    }

    /**
     * @param string $id
     * @return void
     */
    public static function Ban(string $id): void{
        $result = self::$connect->prepare("INSERT INTO t_ban (userid) VALUES (?)");
        $result->execute([$id]);
    }

    /**
     * @param string $email
     * @return mixed
     */
    public static function getUser(string $email): mixed
    {
        $result = self::$connect->prepare("SELECT * FROM user WHERE email = ?");
        $result->execute([$email]);
        return $result->fetchObject();
    }

    /**
     * @param string $id
     * @return mixed
     */
    public static function getUserById(string $id): mixed
    {
        $result = self::$connect->prepare("SELECT * FROM user WHERE id = ?");
        $result->execute([$id]);
        return $result->fetchObject();
    }

    /**
     * @param string $id
     * @return void
     */
    public static function updatelastlogin(string $id): void
    {
        $result = self::$connect->prepare("UPDATE user SET lastlogin = now() WHERE id = ?");
        $result->execute([$id]);
    }

    /**
     * @param string $languageId
     * @return string
     */
    public static function getUserLanguage(string $languageId): string
    {
        $result = self::$connect->prepare("SELECT name FROM language where languageId = ?");
        $result->execute([$languageId]);
        return $result->fetchColumn();
    }

    /**
     * @return mixed
     */
    public static function getLanguageList(): mixed
    {
        $result = self::$connect->prepare("SELECT languageId,name FROM language");
        $result->execute();
        return $result->fetchObject();
    }

    /**
     * @param string $Countryid
     * @return string
     */
    public static function getUserCountry(string $Countryid): string
    {
        $result = self::$connect->prepare("SELECT countryname FROM country WHERE countryId = ?");
        $result->execute([$Countryid]);
        return $result->fetchColumn();
    }

    /**
     * @param string $id
     * @param string $field
     * @param string $value
     * @return void
     */
    public static function updateUserByField(string $id, string $field, string $value): void
    {
        $result = self::$connect->prepare("UPDATE user SET $field = ? WHERE id = ?");
        $result->execute([$value,$id]);
    }

    /**
     * @param string $id
     * @param string $field
     * @return mixed
     */
    public static function SelectByField(string $id, string $field): mixed
    {
        $result = self::$connect->prepare("SELECT $field FROM USER WHERE ID = ?");
        $result->execute([$id]);
        return $result->fetchColumn();
    }

    /**
     * @param string $lang
     * @return string
     */
    public static function getLanguageName(string $lang): string
    {
        $result = self::$connect->prepare("SELECT name FROM language WHERE languageId = ?");
        $result->execute([$lang]);
        return $result->fetchColumn();
    }

    /**
     * @param string $countryId
     * @return string
     */
    public static function getCountryName(string $countryId): string
    {
        $result = self::$connect->prepare("SELECT countryname FROM country WHERE countryId = ?");
        $result->execute([$countryId]);
        return $result->fetchColumn();
    }

}