<?php

namespace app\Controllers;

use app\Helpers\Output;
use app\Models\user;

class year extends Controller
{

    /**
     * @return void
     * Check access for admin only
     * called in formation Dashboard
     * update current year and assign status to finished then insert new year and assign status to on
     */
    public function ModifyCurrentYear():void{

        $user = new user();

        Output::staticRender('navbar');
        Output::staticRender('carousel');
        $year = $this->model->getCurrentYear();

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }

        if(empty($_POST['newyear']) OR empty($_POST['actif'])){
            Output::render('messageBox', 'Veuillez remplir correctement le formulaire');
            return;
        }

        if(!is_numeric($_POST['newyear'])){
            Output::render('messageBox', 'Veuillez indiquer l\'année scolaire uniquement avec des chiffres');
            return;
        }

        if($year >= $_POST['newyear']){
            Output::render('messageBox', 'Veuillez indiquer une année supérieur à l\'année actuel scolaire : '.$year);
            return;
        }

        try {
            $this->model->ModifyCurrentYear($year,$_POST['newyear']);

        } catch (\Exception $e) {

            Output::render('messageBox', 'Year could not be changed');
            return;
        }

        try {
            $this->model->InsertAllYearlyCourse($_POST['newyear']);

        } catch (\Exception $e) {

            Output::render('messageBox', 'Course could not be transfer to '.$_POST['newyear']);
            return;
        }

        Output::render('messageBox', 'Current year has successfuly been changed into '.$_POST['newyear'],'success');
        Output::render('messageBox', 'New '.$_POST['newyear'].' course has been inserted and are availaible','success');

    }


}