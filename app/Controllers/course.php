<?php

namespace app\Controllers;

use app\Helpers\Output;
use app\Models\user;


class course extends Controller
{

    /**
     * @return void
     * called in helper :view (Navbar)
     * generate all active formation list in array and render them in helper :view (formationList)
     */
    public function cursus(): void{

        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        $formationList = $this->model->getFormationList();
        Output::render('formationList',$formationList);
    }

    /**
     * @param string $formationTitle
     * @return void
     * called in inscription button from helper :view (formationList)
     * decode $formationTitle from url and check if the student is not already register inside
     * Register the student in the first branche from the formation
     * rerender formation list with potential alert message
     */
    public function inscription(string $formationTitle):void {

        Output::staticRender('navbar');
        Output::staticRender('carousel');
        $formationList = $this->model->getFormationList();

        $formationTitle = urldecode($formationTitle);
        $firstBranche = $this->model->getFirstBrancheFromFormation($formationTitle);

        if(!$this->model->CheckIfStudentAlreadyRegister($_SESSION['userid'],$firstBranche)){
            $this->model->FormationApplicationFromUser($_SESSION['userid'],$firstBranche);
            Output::render('messageBox', 'Votre demande d\'inscription de la formation : '.$formationTitle.' a été enregistrée ','success');
            Output::render('formationList',$formationList);
        }
        else{
            Output::render('messageBox', 'Erreur : Vous ètes déja enregistré dans la formation : '.$formationTitle);
            Output::render('formationList',$formationList);
        }
    }

    /**
     * @param string $formationTitle
     * @return void
     * called in Remove button from helper :view (formationList)
     * Check access for ADMIN only
     * decode $formationTitle from url and set her inactive
     * rerender formation list with potential alert message
     */
    public function removeFormation(string $formationTitle):void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }

        $formationTitle = urldecode($formationTitle);

        try {
            $this->model->AdminRemoveFormation($formationTitle,0);
        } catch (\Exception $e) {

            Output::render('messageBox', 'Error, Formation did not get removed');
            $formationList = $this->model->getFormationList();
            Output::render('formationList',$formationList);
            return;
        }

        Output::render('messageBox', ' La formation '.$formationTitle.' a été rendue inactive','success');
        $formationList = $this->model->getFormationList();
        Output::render('formationList',$formationList);
    }

    /**
     * @param string $formationTitle
     * @return void
     * called in formation Dashboard from helper :view (AdminFormationDashboard)
     * Check access for ADMIN only
     * Decode $formationTitle from url and set this formation active
     * Rerender formation Dashboard with potential alert message
     */
    public function reAddFormation(string $formationTitle):void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }

        $formationTitle = urldecode($formationTitle);

        try {
            // Second argument (1) from $formationTitle will make formation active
            $this->model->AdminRemoveFormation($formationTitle,1);
        } catch (\Exception $e) {

            Output::render('messageBox', 'Error, Formation did not get remadded');
            $RemovedFormationList = $this->model->getAdminRemovedFormationList();
            Output::render('AdminFormationDashboard',$RemovedFormationList);
            return;
        }
        Output::render('messageBox', ' La formation '.$formationTitle.' a été rétablie','success');
        $RemovedFormationList = $this->model->getAdminRemovedFormationList();
        Output::render('AdminFormationDashboard',$RemovedFormationList);
    }

    /**
     * @param string $CourseName
     * @param int $year
     * @return void
     * called in helper :view (studentCourseList)
     * Check access for Student only
     * Decode $CourseName from url and get id from this yearly_course with $year param
     * Register the student with his userid to the course with id from yearly_course
     * Rerender all availaible course for this user and current year (AvailaibleCourseList)
     */
    public function courseInscription(string $CourseName, int $year):void {

        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }

        if(!$this->model->checkStudentRole($_SESSION['userid'])){
            Output::render('messageBox', 'Only Student are allowed here');
            return;
        }

        $CourseName = urldecode($CourseName);
        $courseId = $this->model->getIdFromCourse($CourseName,$year);

        if($this->model->CheckCourse($_SESSION['userid'],$courseId)){
            Output::render('messageBox', 'You\'re already register in this course');
        }
        else{
            $this->model->AcceptCourse($_SESSION['userid'],$courseId);
        }
        $currentYear = $this->model->getCurrentYear();
        $studentData = $this->model->AvailaibleCourseList($_SESSION['userid'],$currentYear);

        Output::render('studentCourseList',$studentData);
    }

    /**
     * @param string $formationTitle
     * @return void
     *  called in Liste des cours button from helper :view (formationList) with help of javascript(Modal)
     *  Get all courses link to this formation
     *  render them inside the Modal
     */
    public function CourseListByFormation(string $formationTitle): void {

        $formationTitle =urldecode($formationTitle);
        $CourseList = $this->model->GetCourseListByFormation($formationTitle);
        Output::render('CourseListModal', $CourseList);
    }

    /**
     * @param string $formationTitle
     * @return void
     * Check access for ADMIN only
     * called in Add Teacher button from helper :view (formationList) with help of javascript
     * Get all user that are not following this course as student
     * render them with datatable in a Modal
     */
    public function BrancheAddTeacher(string $formationTitle): void {

        $formationTitle =urldecode($formationTitle);

        $teacherList = $this->model->PotentialTeacherList($formationTitle);
        Output::render('PotentialTeacherList',$teacherList,$formationTitle);
    }

    /**
     * @param string $userid
     * @param string $formationTitle
     * @return void
     *  Check access for ADMIN only
     *  Called in Ajouter button from Add teacher MODAL in helper :view (formationList) with help of javascript
     *  Check with $formationTitle AND $userid if teacher not already rerister in this formation
     *  Insert teacher in all branche from the formation $formationTitle
     */
    public function AddTeacherInFormation(string $userid, string $formationTitle): void {

        $user = new user();

        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }

        $formationTitle =urldecode($formationTitle);

        if($this->model->CheckInsertTeacherInFormation($userid,$formationTitle)){
            Output::render('messageBox', 'Teacher already register in this formation');
            $formationList = $this->model->getFormationList();
            Output::render('formationList',$formationList);
            return;
        }

        $this->model->InsertTeacherInFormation($userid,$formationTitle);

        if($this->model->CheckInsertTeacherInFormation($userid,$formationTitle)){
            Output::render('messageBox', 'Teacher has been added','success');
        }


        $formationList = $this->model->getFormationList();
        Output::render('formationList',$formationList);


    }

    /**
     * @param string $formationTitle
     * @return void
     * Render all course from formation $formationTitle In a json file download by client
     */
    public function exportCourse(string $formationTitle) : void {

        session_name('EXAM' . date('Ymd'));
        session_start(['cookie_lifetime' => 3600]);

        $formationTitle = urldecode($formationTitle);
        $courseData = $this->model->GetCourseListByFormation($formationTitle);

        $filename = $_SESSION['userid']. '_' . time() . '.json';
        echo json_encode($courseData);
        header('Content-type: application/json');
        header('Content-disposition: attachment; filename="' . $filename . '"');

    }

    /**
     * @return void
     *  Called in Admin dropdown menu in helper :view (Navbar)
     *  Check access for ADMIN only
     *  Get all student that register for the formation and that are not accepted already
     *  Render them in a datatable
     */
    public function awaitingStudent() : void{

        $user = new user();

        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }

        $studentData = $this->model->getAwaitingStudentList();
        Output::render('awaitingStudentList',$studentData);
    }

    /**
     * @param string $userid
     * @param string $category
     * @param string $branche
     * @return void
     * Called in Accept and Reject button in helper :view (awaitingStudentList)
     * Check access for ADMIN only
     * $category will serve to choice if student have to be accepted or rejected
     * set field "accept" from t_student to 1 if accept , or "reject" field to 1 if $category == reject
     * Rerender awaitingStudentList in datatable
     */
    public function manageStudent(string $userid, string $category, string $branche) : void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }


        if($category == 'accept'){
            $this->model->AcceptStudent($userid,$branche);
            Output::render('messageBox', 'Student with id '.$userid.' has been accepted in '.$branche,'success');
        }
        elseif($category == 'reject'){
            $this->model->RefuseStudent($userid,$branche);
            Output::render('messageBox', 'Student with id '.$userid.' has not been accepted in '.$branche,);
        }

        $studentData = $this->model->getAwaitingStudentList();
        Output::render('awaitingStudentList',$studentData);
    }

    /**
     * @return void
     * Called in Cours dropdown menu in helper :view (Navbar)
     * Check access for Student only
     * Get year and get in array all course from this year and from the branche where the student is register
     * Render them in a datatable
     */
    public function availaibleCourse() : void {

        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }

        if(!$this->model->checkStudentRole($_SESSION['userid'])){
            Output::render('messageBox', 'Only Student are allowed here');
            return;
        }

        $currentYear = $this->model->getCurrentYear();
        $studentData = $this->model->AvailaibleCourseList($_SESSION['userid'],$currentYear);
        Output::render('studentCourseList',$studentData);

    }

    /**
     * @return void
     *  Called in Cours dropdown menu in helper :view (Navbar)
     *  Check access for Student only
     *  Get all course where the student is register (with help of his ID)
     *  Render them in a datatable
     */
    public function myCourse() : void {

        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }

        if(!$this->model->checkStudentRole($_SESSION['userid'])){
            Output::render('messageBox', 'Only Student are allowed here');
            return;
        }

        $currentCourseList = $this->model->CurrentCourseList($_SESSION['userid']);
        Output::render('studentCurentCourseList',$currentCourseList);
    }

    /**
     * @return void
     *  Called in Teacher dropdown menu in helper :view (Navbar)
     *  Check access for Teacher only
     *  Get access to all courses from branche where the teacher is register , and that don't have already a teacher assigned , from the current year
     *  Render them in a datatable
     */
    public function teacherAvailaibleCourseInscription(): void{

        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(empty($this->model->checkTeacherRole($_SESSION['userid']))){
            Output::render('messageBox', 'Only Teacher allowed here');
            return;
        }

        Output::render('messageBox', 'Availaible list of courses (Teacher)','success');
        $year = $this->model->getCurrentYear();
        $courseList = $this->model->TeacherAvailableCourseList($_SESSION['userid'],$year);
        Output::render('TeacherAvailableCourseList',$courseList);


    }

    /**
     * @param string $courseid
     * @return void
     *  Called in "S'inscrire" button from helper :view (TeacherAvailableCourseList)
     *  Check access for Teacher only
     *  Check if $courseid doesn't already have a teacher assigned
     *  Assign the Teacher to the $courseid with help of teacher id ($_SESSION['userid'])
     *  Recheck if the Teacher id has correctly been assigned with the $courseid on yearly_course(table)
     *  Rerender helper :view (TeacherAvailableCourseList)
     */
    public function TeacherCourseInscription(string $courseid): void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(empty($this->model->checkTeacherRole($_SESSION['userid']))){
            Output::render('messageBox', 'Only Teacher allowed here');
            return;
        }

        $check = $this->model->CheckCourseTeacher($courseid);
        if(empty($check)){
            Output::render('messageBox', 'This course have already a teacher');
            return;
        } else{
            $this->model->InsertTeacherInCourse($_SESSION['userid'],$courseid);

            if(!empty($this->model->CHECKInsertTeacherInCourse($_SESSION['userid'],$courseid)) AND !empty($this->model->CHECKInsertTeacherInYearlyCourse($_SESSION['userid'],$courseid))){
                Output::render('messageBox', 'YOU HAVE BEEN REGISTER','success');
            }
            else{
                Output::render('messageBox', 'ERROR - REGISTRATION NOT DONE',);
            }
        }

        $year = $this->model->getCurrentYear();
        $courseList = $this->model->TeacherAvailableCourseList($_SESSION['userid'],$year);
        Output::render('TeacherAvailableCourseList',$courseList);
    }

    /**
     * @return void
     *  Called in Teacher dropdown menu in helper :view (Navbar)
     *  Check access for Teacher only
     *  Get array of course where teacher id($_SESSION['userid']) is register
     *  render array in datatable - helper :view (TeacherCourseList)
     */
    public function TeacherMyCourse(): void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(empty($this->model->checkTeacherRole($_SESSION['userid']))){
            Output::render('messageBox', 'Only Teacher allowed here');
            return;
        }

        Output::render('messageBox', 'Courses list (Teacher)','success');
        $year = $this->model->getCurrentYear();
        $teacherList = $this->model->getTeacherCourseList($_SESSION['userid'],$year);
        Output::render('TeacherCourseList',$teacherList);

    }

    /**
     * @param string $YearlyCourseId
     * @return void
     *  Called in "Student" button from helper :view (TeacherCourseList)
     *  Check access for Teacher only
     *  Get array of all student from course's button
     *  render array in datatable IN A MODAL (Javascript)
     */
    public function TeacherMyCourseModal(string $YearlyCourseId): void{

        session_name('EXAM' . date('Ymd'));
        session_start(['cookie_lifetime' => 3600]);

        if(empty($_SESSION['userid'])){
            return;
        }
        if(empty($this->model->checkTeacherRole($_SESSION['userid']))){
            return;
        }

        $studentList = $this->model->getAllStudentFromCourseId($YearlyCourseId);
        Output::render('TeacherCourseListModal',$studentList);
    }

    /**
     * @param string $courseid
     * @param string $studentid
     * @return void
     *  Called in "Modifier" button from Student list MODAL in helper :view (TeacherCourseList)
     *  Check access for Teacher only
     *  Update student note with help of $courseid AND $studentid
     *  Student new note is pass by $_POST ['StudentNote']
     *  Rerender array for helper :view (TeacherCourseList)
     */
    public function ModifyStudentNote(string $courseid, string $studentid): void {


        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(empty($this->model->checkTeacherRole($_SESSION['userid']))){
            Output::render('messageBox', 'Only Teacher allowed here');
            return;
        }

        if(empty($_POST['StudentNote'])){
            Output::render('messageBox', 'ERROR, No number find for changing note');
            return;
        }


        if($this->model->UpdateStudentNote($studentid,$courseid,$_POST['StudentNote'])){
            Output::render('messageBox', 'Note have been changed','success');
        }

        $year = $this->model->getCurrentYear();
        $teacherList = $this->model->getTeacherCourseList($_SESSION['userid'],$year);
        Output::render('TeacherCourseList',$teacherList);

    }

    /**
     * @param string $courseid
     * @return void
     *  Called in helper :view (TeacherCourseList) FROM "End Course" button
     *  Check access for Teacher only
     *  Update Course status field to 1 in yearly_course
     *  Check with a select count(sql) if the course has been successfully ended
     */
    public function EndCourse(string $courseid): void{

        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(empty($this->model->checkTeacherRole($_SESSION['userid']))){
            Output::render('messageBox', 'Only Teacher allowed here');
            return;
        }

        if(empty($this->model->EndCourse($courseid))){
            output::render('messageBox', 'Course have been ended successfully','success');
        }


        $year = $this->model->getCurrentYear();
        $teacherList = $this->model->getTeacherCourseList($_SESSION['userid'],$year);
        Output::render('TeacherCourseList',$teacherList);
    }

    /**
     * @param string $courseid
     * @return void
     *  Called in helper :view (TeacherCourseList) FROM "Reject" button
     *  Check access for Teacher only
     *  Unlink Course from $courseid with id of Teacher $_SESSION['userid']
     *  Rerender helper :view (TeacherCourseList)
     */
    public function TeacherRejectCourse(string $courseid):void{

        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(empty($this->model->checkTeacherRole($_SESSION['userid']))){
            Output::render('messageBox', 'Only Teacher allowed here');
            return;
        }

        $this->model->TeacherRejectCourse($courseid,$_SESSION['userid']);

        $year = $this->model->getCurrentYear();
        $teacherList = $this->model->getTeacherCourseList($_SESSION['userid'],$year);
        Output::render('TeacherCourseList',$teacherList);


    }

    /**
     * @return void
     *  Called in Admin dropdown menu in helper :view (Navbar)
     *  Check access for ADMIN only
     *  Get array of all teacher and their formation
     *  Render array in datatable : helper :view (AdminTeacherList)
     */
    public function AdminTeacherList(): void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }


        $TeacherList = $this->model->TeacherListForAdmin();
        Output::render('AdminTeacherList',$TeacherList);


    }

    /**
     * @return void
     *  Called in Admin dropdown menu in helper :view (Navbar)
     *  Check access for ADMIN only
     *  Get array of all student and their formation/branche
     *  Render array in datatable : helper :view (AdminStudentList)
     */
    public function AdminStudentList(): void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }

        $StudentList = $this->model->StudentListForAdmin();
        Output::render('AdminStudentList',$StudentList);


    }

    /**
     * @param string $studentId
     * @param string $Branche
     * @return void
     *  Called in helper :view (AdminStudentList) From "Note" button
     *  Check access for ADMIN only
     *  Get array of all student data from course(note,etc)
     *  Render array in datatable from MODAL send by javascript
     */
    public function AdminStudentListModal(string $studentId, string $Branche):void{

        $user = new user();
        session_name('EXAM' . date('Ymd'));
        session_start(['cookie_lifetime' => 3600]);

        if(empty($_SESSION['userid'])){
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }

        $StudentCourseFromBranche = $this->model->StudentListForAdmin_CourseModal($studentId,$Branche);
        Output::render('AdminStudentList_Coursemodal',$StudentCourseFromBranche);
    }

    /**
     * @param $teacherId
     * @param $FormationTitle
     * @param $action
     * @return void
     *  Called in helper :view (AdminStudentList) From "Remove" OR "Rajouter" button
     *  Check access for ADMIN only
     *  decode $FormationTitle from url passing
     *  $action send if action should be to add or remove the Teacher
     *  Unlink all course linked with the Teacher ID and make them availaible
     */
    public function AdminRejectTeacher($teacherId, $FormationTitle, $action):void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }

        $FormationTitle = urldecode($FormationTitle);

        if($action == 'remove'){
            try {
                $this->model->RemoveTeacher($teacherId,$FormationTitle);
            } catch (\Exception $e) {

                Output::render('messageBox', 'Error, change not done');
                $TeacherList = $this->model->TeacherListForAdmin();
                Output::render('AdminTeacherList',$TeacherList);
                return;
            }
            Output::render('messageBox', 'Teacher has been removed from this formation : '.$FormationTitle,'success');
        } elseif ($action =='add'){

            try {
                $this->model->ReAddTeacher($teacherId,$FormationTitle);
            } catch (\Exception $e) {

                Output::render('messageBox', 'Error, change not done');
                $TeacherList = $this->model->TeacherListForAdmin();
                Output::render('AdminTeacherList',$TeacherList);
                return;
            }
            Output::render('messageBox', 'Teacher has been readded to this formation : '.$FormationTitle,'success');
        }

        $TeacherList = $this->model->TeacherListForAdmin();
        Output::render('AdminTeacherList',$TeacherList);

    }

    /**
     * @param $studentId
     * @param $Branchename
     * @param $action
     * @return void
     *  Called in helper :view (AdminTeacherList) From "Remove" OR "Rajouter" button
     *  Check access for ADMIN only
     *  decode $FormationTitle from url passing
     *  $action send if action should be to add or remove the Student
     */
    public function AdminRejectStudent($studentId, $Branchename, $action):void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }

        $Branchename = urldecode($Branchename);

        if($action =='remove'){

            try {
                $this->model->RemoveStudent($studentId,$Branchename,1);
            } catch (\Exception $e) {

                Output::render('messageBox', 'Error, change not done');
                $StudentList = $this->model->StudentListForAdmin();
                Output::render('AdminStudentList',$StudentList);
                return;
            }
            Output::render('messageBox', 'Student has been removed','success');
        }
        elseif($action =='add'){

            try {
                $this->model->RemoveStudent($studentId,$Branchename,0);
            } catch (\Exception $e) {

                Output::render('messageBox', 'Error, change not done');
                $StudentList = $this->model->StudentListForAdmin();
                Output::render('AdminStudentList',$StudentList);
                return;
            }
            Output::render('messageBox', 'Student has been added back','success');
        }
        $StudentList = $this->model->StudentListForAdmin();
        Output::render('AdminStudentList',$StudentList);
    }

    /**
     * @param string $studentId
     * @param string $newBrancheName
     * @param string $OldBrancheName
     * @return void
     *  Called in helper :view (AdminStudentList) From "Validate Year" button
     *  Check access for ADMIN only
     *  Validate student branche with student id AND $OldBrancheName with "finished" field from t_student table set to 1
     *  Insert into t_student table $studentId and link him with $newBrancheName
     *  Rerender helper :view (AdminStudentList)
     */
    public function AdminUpgradeStudent(string $studentId, string $newBrancheName, string $OldBrancheName):void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }

        try {
            $this->model->AdminValidateStudentBranche($studentId,$OldBrancheName);
        } catch (\Exception $e) {
            Output::render('messageBox', 'Error with student validation');

            $StudentList = $this->model->StudentListForAdmin();
            Output::render('AdminStudentList',$StudentList);
            return;
        }

        try {
            $this->model->AdminRegisterStudentNewBranche($studentId,$newBrancheName);
        } catch (\Exception $e) {
            Output::render('messageBox', 'Error with student new inscription with in '.$newBrancheName);

            $StudentList = $this->model->StudentListForAdmin();
            Output::render('AdminStudentList',$StudentList);
            return;
        }
        Output::render('messageBox', 'Student has been successfully transfer in '.$newBrancheName,'success');
        $StudentList = $this->model->StudentListForAdmin();
        Output::render('AdminStudentList',$StudentList);
    }

    /**
     * @return void
     *  Called in Formation(Admin) navbar item in helper :view (Navbar)
     *  Check access for ADMIN only
     *  Get array of all formation that have "status" field set to 0
     *  Render them in datatable with helper :view (AdminFormationDashboard)
     */
    public function AdminFormationDashboard():void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }
        $RemovedFormationList = $this->model->getAdminRemovedFormationList();
        Output::render('AdminFormationDashboard',$RemovedFormationList);

    }

    /**
     * @return void
     *  Called in helper :view (AdminFormationDashboard) From Form validate action button
     *  Check access for ADMIN only
     *  Test $_POST and verify entered data
     *  Insert into Formation table data
     *  Insert into branche table correct number of branche depend of $_POST['time'] and give them unique name
     *  Rerender helper :view (AdminFormationDashboard)
     */
    public function AddFormation():void{

        $user = new user();
        Output::staticRender('navbar');
        Output::staticRender('carousel');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$user->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Admin only');
            return;
        }
        if(!empty($_POST['formationTitle']) AND !empty($_POST['Level']) AND !empty($_POST['time'])AND !empty($_POST['desc']) AND !empty($_POST['firstbranche'])){
            if(is_numeric($_POST['formationTitle']) OR is_numeric($_POST['Level']) OR is_numeric($_POST['desc'])){

                Output::render('messageBox', 'ERROR : Formation title, level or desciption can not be only numeric');
                $RemovedFormationList = $this->model->getAdminRemovedFormationList();
                Output::render('AdminFormationDashboard',$RemovedFormationList);
                return;
            }
            if(!empty($this->model->CHECKIfFormationTitleAvailaible($_POST['formationTitle']))){

                Output::render('messageBox', 'This formation title is already take');
                $RemovedFormationList = $this->model->getAdminRemovedFormationList();
                Output::render('AdminFormationDashboard',$RemovedFormationList);
                return;
            }
            if($_POST['time'] > 8){

                Output::render('messageBox', 'Formation can not last longer than 8 year');
                $RemovedFormationList = $this->model->getAdminRemovedFormationList();
                Output::render('AdminFormationDashboard',$RemovedFormationList);
                return;
            }

            try {
                $this->model->AdminAddFormation($_POST['formationTitle'],$_POST['Level'],$_POST['time'],$_POST['desc']);

            } catch (\Exception $e) {

                Output::render('messageBox', 'Error with formation validation');
                $RemovedFormationList = $this->model->getAdminRemovedFormationList();
                Output::render('AdminFormationDashboard',$RemovedFormationList);
                return;
            }

            for ($i = 1; $i <= $_POST['time']; $i++) {

                $loopbranchename = $_POST['firstbranche'] . $i;

                if($i == 1){
                    $this->model->AdminAddFirstBrancheFromFormation($_POST['formationTitle'],$loopbranchename,$i);
                }
                else {

                    $oldbranchename = substr($loopbranchename, 0, -1);
                    $oldbranchename = $oldbranchename .($i-1);
                    $this->model->AdminAddFirstBrancheFromFormation($_POST['formationTitle'],$loopbranchename,$i,$oldbranchename);
                }
            }
            Output::render('messageBox', 'Formation '.$_POST['formationTitle'].' has been added to the Website - Activation in formation dashboard','success');
        }else{
            Output::render('messageBox', 'Please enter information on all label');
        }

        $RemovedFormationList = $this->model->getAdminRemovedFormationList();
        Output::render('AdminFormationDashboard',$RemovedFormationList);
    }


}
