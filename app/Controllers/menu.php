<?php

namespace app\Controllers;



use app\Helpers\Output;
use app\Models\user;

class menu
{
    public function navbarAndCarousel(): void
    {

        $user = new user();
        $language =$user->getLanguageList();

        Output::render('navbar',$language);
        output::staticRender('carousel');
    }


    public function home(): void
    {
        self::navbarAndCarousel();
        echo file_get_contents(ROOT_PATH . '/view/component/goal.html');
        echo file_get_contents(ROOT_PATH . '/view/component/lastnews.html');
        echo file_get_contents(ROOT_PATH . '/view/component/location.html');
    }


    public function galery(): void
    {
        self::navbarAndCarousel();
        echo file_get_contents(ROOT_PATH . '/view/component/galery.html');

    }












}