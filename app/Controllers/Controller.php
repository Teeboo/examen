<?php

namespace app\Controllers;
use stdClass;
use app\Models\Model;
abstract class Controller
{
    protected int $id;
    protected mixed $data;
    protected Model $model;
    protected static mixed $static_model = null;



    public function __construct(int $id = null)
    {
        $class = get_called_class();
        $data = explode('\\', $class);
        $class = '\app\Models\\' . end($data);
        $this->model = new $class();
        self::$static_model = $this->model;
        if (!empty($id)) {
            $this->id = $id;
            $this->data = $this->model->get($id);
        }
    }


    /**
     * Récupère, sous forme d'objet, le contenu du record en DB sur base de son id
     *
     * @param int $id
     * @return mixed
     */
    public function get(int $id): mixed
    {
        return $this->model->getByField('id', $id);
    }

    /**
     * Injecte les propriétés d'un objet comme propriétés de l'objet instancié du Controller
     *
     * @param stdClass $object
     * @return void
     */
    public function import(stdClass $object): void
    {
        foreach (get_object_vars($object) as $key => $value) {
            $this->$key = $value;
        }
    }







}