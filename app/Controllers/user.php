<?php
namespace app\Controllers;

use app\Helpers\Output;
use \app\Models\role;


class user extends Controller
{

    /**
     * @var array|string[]
     * Array if string
     * Determine which file extention will be usable in file transfer from user
     */
    protected static array $allowedImages = [
        'image/jpeg',
        'image/jpg',
        'image/png',
    ];

    //TODO = Check if phone number not too long

    /**
     * @return void
     *  Called in "Connexion" dropdown navbar menu FROM helper :view (Navbar)
     *  Check $_POST integrity data
     *  Compare $_POST data with database data for unique field
     *  Create object $user_data and fill it with $_POST data
     *  Insert $_POST data in user table
     *  CHECK rowcount added in user table
     *  output validation or error message
     */
    public function create(): void{

        self::requestCheck($_POST);

        if (!in_array($_POST['lang'], ['en', 'fr'])) {
            self::renderFullMenu();
            Output::render('messageBox', 'Veuillez choisir une langue disponible uniquement');
            return ;
        }

        if(!is_numeric($_POST['phone'])){
            self::renderFullMenu();
            Output::render('messageBox', 'Veuillez utiliser un format de numéro de téléphone correcte');
            Output::staticRender('homemenu');
            return ;
        }

        if (!empty($_POST['last-name']) && !empty($_POST['first-name']) && !empty($_POST['lang']) && !empty($_POST['password']) && !empty($_POST['username']) && !empty($_POST['passverify'])
            && !empty($_POST['Adresse']) && !empty($_POST['country']) && !empty($_POST['phone']) && !empty($_POST['email'])){

            if(strcmp($_POST['password'], $_POST['passverify']) !== 0){
                self::renderFullMenu();
                Output::render('messageBox', 'Vos mot de passe ne correspondent pas');
                Output::staticRender('homemenu');
                return;
            }
            else{

                if( is_numeric($_POST['last-name']) OR  is_numeric($_POST['first-name']) OR is_numeric($_POST['Adresse'])){
                    self::renderFullMenu();
                    Output::render('messageBox', 'Erreur : Nom,prénom et adresse ne peuvent contenir uniquement des chiffres');
                    Output::staticRender('homemenu');
                    return;
                }


                if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                    self::renderFullMenu();
                    Output::render('messageBox', 'Votre adresse email est incorrecte');
                    Output::staticRender('homemenu');
                    return;
                }

                if(!empty($_POST['news'])){
                    $_POST['news']=1;
                }
                else{
                    $_POST['news']=0;
                }

                $user_data = [
                    'lastname' => trim($_POST['last-name']),
                    'firstname' => trim($_POST['first-name']),
                    'address' => trim($_POST['Adresse']),
                    'username' => trim($_POST['username']),
                    'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
                    'email' => $_POST['email'],
                    'lang' => $_POST['lang'],
                    'countryid' => trim($_POST['country']),
                    'phone' => trim($_POST['phone']),
                    'news' =>$_POST['news']
                ];

                if($this->model->checkIfUsernameExist($user_data['username'])){
                    self::renderFullMenu();
                    Output::render('messageBox', 'L\'utilisateur '.$user_data['username'].' existe déja');
                    Output::staticRender('homemenu');
                    return ;
                }

                if($this->model->checkIfEmailExist($user_data['email'])){
                    self::renderFullMenu();
                    Output::render('messageBox', 'L\'adresse email '.$user_data['email'].' existe déja');
                    Output::staticRender('homemenu');
                    return ;
                }

                try {
                    $userid = $this->model->create($user_data);
                } catch (\Exception $e) {
                    self::renderFullMenu();
                    Output::render('messageBox', 'Echec de la création de votre compte',);
                    return ;
                }


                if ($userid) {
                    $role = new role();
                    self::renderFullMenu();
                    $role->AddRoleGuest($userid);
                    Output::render('messageBox', 'Création de votre compte réussie',"success");
                    Output::staticRender('homemenu');

                }
                else{
                    self::renderFullMenu();
                    Output::render('messageBox', 'Échec de la création de votre compte');
                    Output::staticRender('homemenu');
                }
            }
        }
        else {
            self::renderFullMenu();
            Output::render('messageBox', 'Échec de la création de votre compte, Veuillez remplir entièrement le formulaire');
            Output::staticRender('homemenu');
        }
    }

    /**
     * @return void
     *  Called in "Connexion" dropdown navbar menu FROM helper :view (Navbar)
     *  Check existance of $_POST data in user table
     *  Verify password with native php function password_verify
     *  Destroy current user session
     *  Create new user session AND fill $_SESSION with userid from user table
     *  render home menu
     */
    public function login(): void{

        self::requestCheck($_POST);

        if(!empty($_POST['email']) && !empty($_POST['password']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){

            if(!$this->model->checkIfEmailExist($_POST['email'])){
                self::renderFullMenu();
                Output::render('messageBox', 'Adresse email inconnue');
                Output::staticRender('homemenu');
            }
            else {
                $user= $this->model->getUser($_POST['email']);

                if(password_verify($_POST['password'], $user->password)) {

                    if($this->model->CheckBan($user->id)){
                        self::renderFullMenu();
                        Output::render('messageBox', 'Votre compte est bani, connexion impossible');
                        return;
                    }

                    session_destroy();
                    session_name('EXAM' . date('Ymd'));
                    session_id(bin2hex(openssl_random_pseudo_bytes(32)));
                    session_start(['cookie_lifetime' => 3600]);

                    $_SESSION['userid'] = $user->id;
                    $_SESSION['lang'] = $user->lang;

                    self::renderFullMenu();
                    Output::render('messageBox', 'Réussite de la connexion',"success");
                    Output::staticRender('homemenu');
                    $this->model->updatelastlogin($_SESSION['userid']);
                }
                else{
                    self::renderFullMenu();
                    Output::render('messageBox', 'Il semble que votre mot de passe n’est pas valide. Veuillez le corriger et essayer à nouveau.');
                    Output::staticRender('homemenu');
                }
            }
        }
    }

    /**
     * @return void
     *  Called in "Avatar image" dropdown menu FROM helper :view (Navbar) when connected
     *  Destroy session
     *  Render home menu
     */
    public function logout(): void{
        session_unset();
        session_destroy();
        session_write_close();

        self::renderFullMenu();
        Output::staticRender('homemenu');
    }

    /**
     * @return void
     * @throws \Exception
     *  Called in "Avatar image" dropdown menu FROM helper :view (Navbar) when connected
     *  Check if user still connected
     *  Get userdata as object from his userid
     *  Formating userdata object for profil
     *  Rendering userdata object with helper :view (profil)
     */
    public function profil(): void{

        self::renderFullMenu();

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }

        $userContent= $this->model->getUserById($_SESSION['userid']);

        unset($userContent->password);
        unset($userContent->created);
        unset($userContent->updated);

        if(!empty($userContent->birthday)){
            $userContent->birthday = date_format( new \DateTime($userContent->birthday),"d/m/Y");
        }

        $userContent->countryid = $this->model->getUserCountry($userContent->countryid);
        $userContent->lang= $this->model->getUserLanguage($userContent->lang);


        if($userContent->birthday ==null){
            $userContent->birthday='Inconnue';
        }

        Output::render('profil',$userContent);
    }

    /**
     * @param string $category
     * @return void
     *  Called in Profil page in MODAL from button "Modifier"
     *  $category will set which data $_POST is from
     *  Verify $_POST integrity data
     *  Update from user table data with $_POST
     *  Rerender home menu
     */
    public function modifyUser(string $category): void{

        if($category!='image'){
            self::renderFullMenu();
        }

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        } else {
            $user= $this->model->getUserById($_SESSION['userid']);
        }

        if($category=='pers'){

            self::requestCheck($_POST);
            if(empty($_POST['first-name']) and empty($_POST['last-name']) and empty($_POST['email']) and empty($_POST['username'])){
                Output::render('messageBox', 'Aucune modification demandée');
                output::staticRender('homemenu');
                return;
            }

            if(!empty($_POST['last-name']) AND $_POST['last-name']!=$user->lastname){

                if( is_numeric($_POST['last-name'])){

                    Output::render('messageBox', 'Erreur : Votre nom ne peut contenir que des chiffres');
                    Output::staticRender('homemenu');
                    return;
                }
                $this->model->updateUserByField($_SESSION['userid'],'lastname',$_POST['last-name']);
                Output::render('messageBox', 'Votre nom de famille a bien été modifié en : '.$_POST['last-name'],"success");
            }
            if(!empty($_POST['first-name']) AND $_POST['first-name']!=$user->firstname){
                if( is_numeric($_POST['first-name'])){

                    Output::render('messageBox', 'Erreur : Votre prénom ne peut contenir que des chiffres');
                    Output::staticRender('homemenu');
                    return;
                }

                $this->model->updateUserByField($_SESSION['userid'],'firstname',$_POST['first-name']);
                Output::render('messageBox', 'Votre prénom a bien été modifié en : '.$_POST['first-name'],"success");
            }
            if(!empty($_POST['email']) AND $_POST['email']!=$user->email){
                if($this->model->checkIfEmailExist($_POST['email'])){
                    Output::render('messageBox', 'L\'adresse email '.$_POST['email'].' existe déja');
                } else {
                    if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                        Output::render('messageBox', 'Votre adresse email est incorrecte');
                        Output::staticRender('homemenu');
                        return;
                    }

                    $this->model->updateUserByField($_SESSION['userid'],'email',$_POST['email']);
                    Output::render('messageBox', 'Votre adresse mail a bien été modifié en : '.$_POST['email'],"success");
                }

            }
            if(!empty($_POST['username']) AND $_POST['username']!=$user->username){
                if($this->model->checkIfUsernameExist($_POST['username'])){
                    Output::render('messageBox', 'L\'utilisateur '.$_POST['username'].' existe déja');
                } else {
                    $this->model->updateUserByField($_SESSION['userid'],'username',$_POST['username']);
                    Output::render('messageBox', 'Votre nom utilisateur a bien été modifié en : '.$_POST['username'],"success");
                }
            }
            output::staticRender('homemenu');
        }

        if($category=='adress'){

            self::requestCheck($_POST);
            if(empty($_POST['Adresse']) and empty($_POST['country']) and empty($_POST['phone'])){
                Output::render('messageBox', 'Aucune modification demandée');
                output::staticRender('homemenu');
                return;
            }

            if(!empty($_POST['Adresse']) AND $_POST['Adresse']!=$user->address){

                $this->model->updateUserByField($_SESSION['userid'],'address',$_POST['Adresse']);
                Output::render('messageBox', 'Votre Adresse a bien été modifié en : '.$_POST['Adresse'],"success");
            }

            if(!empty($_POST['country']) AND $_POST['country']!=$user->countryid){
                $this->model->updateUserByField($_SESSION['userid'],'countryid',$_POST['country']);
                Output::render('messageBox', 'Votre Pays a bien été modifié en : '.$this->model->getCountryName($_POST['country']),"success");
            }

            if(!empty($_POST['phone']) AND $_POST['phone']!=$user->phone){

                if(is_numeric($_POST['phone'])){
                    $this->model->updateUserByField($_SESSION['userid'],'phone',$_POST['phone']);
                    Output::render('messageBox', 'Votre numéro de téléphone a bien été modifié en : '.$_POST['phone'],"success");
                }else{
                    Output::render('messageBox', 'Veuillez rentrer un numéro de téléphone correcte (ne doit contenir que des chiffres) ');
                }
            }
            output::staticRender('homemenu');
        }

        if($category=='optional'){

            self::requestCheck($_POST);
            if(empty($_POST['birthday']) and empty($_POST['lang']) and empty($_POST['theme'])){
                Output::render('messageBox', 'Aucune modification demandée');
                output::staticRender('homemenu');
                return;
            }

            if(!empty($_POST['birthday'])){
                $this->model->updateUserByField($_SESSION['userid'],'birthday',$_POST['birthday']);
                Output::render('messageBox', 'Votre date de naissance a bien été modifié en : '.$_POST['birthday'],"success");
            }

            if(!empty($_POST['lang']) AND $_POST['lang']!=$user->lang){
                $this->model->updateUserByField($_SESSION['userid'],'lang',$_POST['lang']);
                Output::render('messageBox', 'Votre langue a bien été modifié en : '.$this->model->getLanguageName($_POST['lang']),"success");
            }

            if(!empty($_POST['theme']) AND $_POST['theme']!=$user->theme){
                $this->model->updateUserByField($_SESSION['userid'],'theme',$_POST['theme']);
                Output::render('messageBox', 'Votre thème a bien été modifié en : '.$_POST['theme'],"success");
            }
            output::staticRender('homemenu');
        }

        if($category=='image'){

            $file = $_FILES['photo'];
            $filename =$_FILES['photo']['name'];
            $filesize =$_FILES['photo']['size'];
            $filetype = $_FILES['photo']['type'];
            $fileTempLoc = $_FILES['photo']['tmp_name'];
            $fileError = $_FILES['photo']['error'];
            $fileExt = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION);


            if(!empty($filename) && !empty($fileTempLoc) && in_array($filetype, self::$allowedImages) && is_uploaded_file($_FILES['photo']['tmp_name'])){

                if($fileError===0){

                    $imageUrl = 'image' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR;
                    $imagepath = ROOT_PATH . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR;
                    $fileNewName = $_SESSION['userid'].'.'.$fileExt;
                    $dest = $imagepath.$fileNewName;

                    $check = $this->model->SelectByField($_SESSION['userid'],'image');

                    if(!empty($check)){
                        if(file_exists($check)){
                            unlink($check);
                        }
                    }


                    $move = move_uploaded_file($fileTempLoc, $dest);
                    if ($move) {

                        $this->model->updateUserByField($_SESSION['userid'],'image',$imageUrl.$fileNewName);
                        self::renderFullMenu();
                        Output::render('messageBox', 'Votre image de profil a bien été modifié',"success");
                        output::staticRender('homemenu');

                    }

                } else {

                    if($fileError=='UPLOAD_ERR_FORM_SIZE' OR $fileError=='UPLOAD_ERR_INI_SIZE'){
                        Output::render('messageBox', 'Fichier trop volumineux');
                        output::staticRender('homemenu');
                    }

                }

            } else{
                self::renderFullMenu();
                if(empty($filename)){
                    Output::render('messageBox', 'Aucune photo sélectionnée, veuillez recommencer');
                    output::staticRender('homemenu');
                }
                if(!in_array($filetype, self::$allowedImages)){
                    Output::render('messageBox', 'Cette extention de fichier : '.$fileExt.' ne\'st pas autorisée');
                    output::staticRender('homemenu');
                }
                if(empty($fileTempLoc)){
                    Output::render('messageBox', 'Echec d\'exportation de votre image');
                    output::staticRender('homemenu');
                }
            }

        }
    }

    /**
     * @return void
     *  Called in Profil page from button "Modifier"
     *  Check if user have already an image set in user table
     *  remove old image from image/user/photo/id of user find in user table
     *  Set image field in user to null
     *  rerender home menu
     */
    public function removeImage():void{

        $check = $this->model->SelectByField($_SESSION['userid'],'image');

        if(!empty($check)){
            if(file_exists($check)){
                unlink($check);
            }
        }
        $this->model->updateUserByField($_SESSION['userid'],'image','NULL');
        self::renderFullMenu();
        Output::render('messageBox', 'Votre image a bien été supprimée',"success");
        output::staticRender('homemenu');

    }

    /**
     * @param string $category
     * @param string $id
     * @return void
     *  Called in "Admin - Liste user" dropdown navbar menu FROM helper :view (Navbar)
     *  Check access for ADMIN only
     *  Check if selected id for ban is not admin
     *  $category will set if action is to ban or unban userid
     *  Rerender helper :view (userlist)
     */
    public function ManageBan(string $category, string $id):void {

        self::renderFullMenu();

        if(!$this->isloggin()){
            Output::render('messageBox', 'Veuillez vous connectez');
            return;
        }

        if(!$this->isAdmin()){
            Output::render('messageBox', 'Accès uniquement disponible aux administrateurs');
            return;
        }

        if($this->model->CheckAdmin($id)){
            Output::render('messageBox', 'Vous ne pouvez bannir un administrateur');
            $usersdata = $this->model->getUserList();
            Output::render('userlist',$usersdata);
            return;
        }

        if($category=='Ban'){
            $this->model->Ban($id);
            Output::render('messageBox', 'User has been banned','success');
        }
        if($category=='Unban'){
            $this->model->Unban($id);
            Output::render('messageBox', 'User has been unbanned','success');
        }
        $usersdata = $this->model->getUserList();
        Output::render('userlist',$usersdata);


    }

    /**
     * @param string $category
     * @return void
     *  Called in Profil page from button "Exporter"
     *  $category set which part of user data has to be export
     *  Create a .json file that is downloaded by client
     *
     */
    public function export(string $category):void{

        session_name('EXAM' . date('Ymd'));
        session_start(['cookie_lifetime' => 3600]);

        if(!$this->isloggin()){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }

        $userContent= $this->model->getUserById($_SESSION['userid']);
        $userdata = new \stdClass();
        $userdata->username=$userContent->username;

        if($category=='pers'){

            $userdata->id=$userContent->id;
            $userdata->lastname=$userContent->lastname;
            $userdata->firstname=$userContent->firstname;
            $userdata->email=$userContent->email;
            echo json_encode($userdata);
            $filename = $userdata->username . '_' . time() . '.json';
            header('Content-type: application/json');
            header('Content-disposition: attachment; filename="' . $filename . '"');
        }
        if($category=='adress'){

            $userdata->address=$userContent->address;
            $userdata->countryid=$userContent->countryid;
            $userdata->phone=$userContent->phone;
            $filename = $userdata->username . '_Optional_' . time() . '.json';
            echo json_encode($userdata);
            header('Content-type: application/json');
            header('Content-disposition: attachment; filename="' . $filename . '"');
        }
    }

    /**
     * @return void
     *  Called in Admin dropdown menu in helper :view (Navbar)
     *  Check access for ADMIN only
     *  Get user list as array and render it in datatable
     */
    public function userList():void{

        self::renderFullMenu();
        if(!$this->isloggin()){

            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }

        if(self::isAdmin()){

            $usersdata = $this->model->getUserList();
            Output::render('userlist',$usersdata);
        }
        else{
            Output::render('messageBox', 'Accès uniquement disponible aux administrateurs');
        }
    }

    /**
     * @param array $input
     * @return void
     */
    public function requestCheck(array $input): void{
        if(!$input){
            Output::render('messageBox', 'Veuillez utilisez l\'interface du site uniquement');
        }
    }

    /**
     * @return bool
     * return true if is admin
     */
    public function isAdmin():bool{
        return $this->model->CheckAdmin($_SESSION['userid']);
    }

    /**
     * @return bool
     * return true if user logged in
     */
    public function isloggin():bool{

        if(empty($_SESSION['userid'])){
            return false;
        } else{
            return true;
        }
    }

    /**
     * @return void
     * Render dynamic navbar
     */
    public function renderFullMenu(): void{

        Output::staticRender('navbar');
        Output::staticRender('carousel');
    }

}