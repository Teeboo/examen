<?php

namespace app\Helpers;
use app\Models\user;
use app\Models\menu;
use app\Models\course;
class view
{

    /**
     * @param string $message
     * @param string $class
     * @return string
     * return messagebox used for alter with $class string
     */
    public static function messageBox(string $message, string $class = 'danger'): string
    {
        return '<div class=" m-0 blured text-center alert alert-' . $class . '">' . $message . '</div>';
    }

    /**
     * @return string
     * return homepage html content as a string
     */
    public static function homemenu(): string
    {

        $body="";
        $body=file_get_contents(ROOT_PATH .'/view/component/goal.html').file_get_contents(ROOT_PATH .'/view/component/lastnews.html').file_get_contents(ROOT_PATH .'/view/component/location.html');
        return $body;

    }

    /**
     * @param object $data
     * @return string
     */
    public static function profil(object $data): string
    {

        $body='
            <div class="espace"></div> 
                <div class="">
                    <div class="container">
                        <section class="row nopadding">
                            <div class="col-md-3 nopadding">
                                <form action = "index.php?view=api/user/modifyUser/image" method="post" enctype="multipart/form-data"> ';

        if(!empty($data->image) AND $data->image != 'NULL'){
            $body.=' <img src="'.$data->image.'" alt="Bootstrap" class ="pb-3 img-fluid rounded " width="200" height="200">';
        }
        else{
            $body.=' <img src="image/user/photo/Default.png" alt="Bootstrap" class ="pb-3 img-fluid">';
        }


        $body.='
                                    <label for="photo"></label>
                                    <p><input type="file" id="photo" name="photo"></p>  
                                    <div>
                                        <button type="submit" class="btn btn-primary bg-gradient" >Modifier</button>
                                        <a class="btn btn-custom3" href="index.php?view=api/user/removeImage" role="button">Supprimer</a>
                                    </div>                               
                                </form>                           
                            </div>
                           <div class="col-md-1"></div>
                           <div class="col-md-7">
                                <div class="card cardSizing">
                                    <div class="card-body">
                                        <div class="row pb-4">
                                            <div class="col-md-5 pb-4 pt-2">
                                                <h3 class="card-title font">Données personnelles</h3>
                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col pt-2 ">                                             
                                               
                                               <a class="btn btn-primary" href="api/route/user/export/pers" role="button">Exporter</a>
                                               <button type="button" class="btn btn-primary link modal-personnal" data-bs-toggle="modal" data-bs-target="#modal-personnal">Modifier</button>                                              
                                                                                                    
                                            </div>
                                        </div>                                  
                                        <div>
                                            <p class="fs-5 fw-bold nopadding font">'.$data->lastname.' '.$data->firstname.'</p>
                                            <p class="fs-6 nopadding font"> username : '.$data->username.'</p>
                                            <p class="fs-6 pb-4 font"> E-mail : '.$data->email.'</p>
                                            <p class="fs-6 fst-italic font"> Numéro du compte : '.$data->id.'</p>         
                                        </div>                                 
                                    </div>
                                 </div>
                           </div>           
                        </section>
                    </div>
                </div>   
            <div class="espace"></div>
                    
            <div class="espace"></div> 
                <div class="">
                    <div class="container">
                        <section class="row nopadding">
                           <div class="col-md-4"></div>
                           <div class="col-md-7">
                                <div class="card cardSizing">
                                    <div class="card-body">
                                        <div class="row pb-4">
                                            <div class="col-md-5 pb-4 pt-2">
                                                <h3 class="card-title font">Adresse</h3>
                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col pt-2 ">
                                               <a class="btn btn-primary" href="api/route/user/export/adress" role="button">Exporter</a>
                                                <button type="button" class="btn btn-primary link modal-adress" data-bs-toggle="modal" data-bs-target="#modal-adress">Modifier</button>
                                            </div>
                                        </div>
                                        
                                        <div>
                                            <p class="fs-5 nopadding font"> '.$data->address.'</p>
                                            <p class="fs-6 nopadding font"> Pays : '.$data->countryid.'</p>
                                            <p class="fs-6 pb-4 font"> Numéro de téléphone : '.$data->phone.'</p>                                                  
                                        </div>                                
                                    </div>
                                 </div>
                           </div>           
                        </section>
                    </div>
                </div>   
            <div class="espace"></div>
        
            <div class="espace"></div> 
                <div class="">
                    <div class="container">
                        <section class="row nopadding">
                           <div class="col-md-4"></div>
                           <div class="col-md-7">
                                <div class="card cardSizing">
                                    <div class="card-body">
                                        <div class="row pb-4">
                                            <div class="col-md-5 pb-4 pt-2">
                                                <h3 class="card-title font">Optionnel</h3>
                                            </div>
                                            <div class="col-md-5"></div>
                                            <div class="col pt-2 ">
                                                <button type="button" class="btn btn-primary link modal-optional" data-bs-toggle="modal" data-bs-target="#modal-optional">Modifier</button>
                                            </div>
                                        </div>                                      
                                        <div>               
                                            <p class="fs-5 nopadding font"> Date de naissance : '.$data->birthday.'</p>
                                            <p class="fs-6 nopadding font"> Langue : '.$data->lang.'</p>
                                            <p class="fs-6 pb-4 font"> Thème : '.$data->theme.'</p>
                                            <p class="fs-6 fst-italic font"> Dernière connexion : '.$data->lastlogin.'</p>                                                    
                                        </div>                                
                                    </div>
                                 </div>
                           </div>           
                        </section>
                    </div>
                </div>   
            <div class="espace"></div>
        
        <!-- PERS INFO MODAL -->
        <div class="modal fade " tabindex="-1" id="modal-personnal" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content ">
                        <div class="">
                            <section class="row nopadding ">
                                <div class="col-md-5 p-4 nopadding ">
                                    <img src="image/Modal/modal-pers.png" alt="" class="img-fluid w-100">
                                </div>
                                <div class="col-md-7 ps-5 ">
                                    <div class="container-contact nopadding ">
                                        <div class="pb-4 ">
                                            <h4 class="pb-3 display-6 pt-4 font ">Modifier données personnelles</h4>
                                            <div class = "pb-5">
                                                <form action = "index.php?view=api/user/modifyUser/pers" method="post" enctype="multipart/form-data" autocomplete="off" >                              
                                                    <label for="last-name" class ="font" autocomplete="off">Nom de famille</label>
                                                    <input type="text" id="last-name" name="last-name" class="widthform">           
                                                    <label for="first-name" class="font" autocomplete="off">Prénom</label>
                                                    <input type="text" id="first-name" name="first-name" class="widthform">                                                  
                                                    <label for="email" class ="font" autocomplete="off" >E-mail</label>
                                                    <input type="email" id="email" name="email" class="widthform">                                               
                                                     <label for="username" class = "font" autocomplete="off">Identifiant</label>
                                                    <input type="text" id="username" name="username" class="widthform">                                                                         
                                                    <div class ="pt-5">
                                                        <button type="submit" class="btn border-0 btn-primary font btn-lg bg-gradient" >Modifier</button>
                                                    </div>
                                                </form>
                                            </div>            
                                        </div>
                                    </div>
                                </div>         
                            </section>
                        </div>            
                    </div>
                </div>
            </div>
        
        <!-- ADRESS MODAL -->
        <div class="modal fade " tabindex="-1" id="modal-adress" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="">
                            <section class="row nopadding ">
                                <div class="col-md-5 p-4 nopadding ">
                                    <img src="image/Modal/modal-adress.png" alt="" class="img-fluid w-100">
                                </div>
                                <div class="col-md-7 ps-5 ">
                                    <div class="container-contact nopadding ">
                                        <div class="pb-4 ">
                                            <h4 class="pb-3 display-6 pt-4 font ">Modifier Adresse</h4>
                                            <div class = "pb-5">
                                                <form action = "index.php?view=api/user/modifyUser/adress" method="post" enctype="multipart/form-data" autocomplete="off" >                                                                             
                                                    <label for="Adresse" class="font">Adresse</label>
                                                    <input type="text" id="Adresse" name="Adresse" class="widthform">          
                                                    <label for="country" class ="font">Pays</label>
                                                    <select name="country" id="country" class="widthform">
                                                        <option disabled selected value></option>
                                                        <option value="be">Belgique</option>
                                                        <option value="fr">France</option>
                                                        <option value="al">Allemagne</option>
                                                        <option value="ru">Russie</option>
                                                        <option value="ot">Autre</option>
                                                    </select>                                                                     
                                                     <label for="phone" class="font">Numéro de téléphone</label>
                                                    <input type="tel" id="phone" name="phone" class="widthform" maxlength="9">                                                   
                                                    <div class ="pt-5">
                                                        <button type="submit" class="btn border-0 btn-primary font btn-lg bg-gradient" >Modifier</button>
                                                    </div>
                                                </form>
                                            </div>          
                                        </div>
                                    </div>
                                </div>           
                            </section>
                        </div>          
                    </div>
                </div>
            </div>
        <!-- OPTIONAL MODAL -->
        
            <div class="modal fade " tabindex="-1" id="modal-optional" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content bg-custom3">
                        <div class="">
                            <section class="row nopadding ">
                                <div class="col-md-5 p-4  ">
                                    <img src="image/Modal/modal-optional.png" alt="" class="img-fluid w-100">
                                </div>
                                <div class="col-md-7 ps-5 ">
                                    <div class="container-contact nopadding ">
                                        <div class="pb-4 ">
                                            <h4 class="pb-3 display-6 pt-4 font pb-5 ">Modifier Option</h4>
                                            <div class = "pb-5">
                                                <form action = "index.php?view=api/user/modifyUser/optional" method="post" enctype="multipart/form-data" autocomplete="off" >                                                                                 
                                                    <label for="birthday">Date de naissance</label>
                                                    <br>
                                                    <input type="date" id="birthday" name="birthday" value=" " min="1920-01-01" max="2023-01-01">
                                                    <br>                                                                 
                                                    <label for="lang" class ="font">Langue</label>
                                                    <select name="lang" id="lang" class="widthform">
                                                        <option disabled selected value></option>
                                                        <option value="fr">Francais</option>
                                                        <option value="en">Anglais</option>
                                                     </select>                                                  
                                                     <label for="theme" class="font">theme</label>
                                                    <select name="theme" id="theme" class="widthform">
                                                        <option disabled selected value></option>
                                                        <option value="Classic">Classique</option>
                                                     </select>                                                                                                     
                                                    <div class ="pt-5">
                                                        <button type="submit" class=" btn border-0 btn-primary font btn-lg bg-gradient">Modifier</button>
                                                    </div>
                                                </form>
                                            </div>
            
                                        </div>
                                    </div>
                                </div>         
                            </section>
                        </div>            
                    </div>
                </div>
            </div> 
        ';

        return $body;
    }

    /**
     * @param array $data
     * @return string
     */
    public static function userlist(array $data): string{

        $body = '';
        $menu = new menu();
        $user = new user();

        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {


                if($key =='image'){
                    if(empty ($value) OR $value == 'NULL'){
                        $value = 'image\user\photo\Default.png';
                        $body .=  '<td>' . '<img src="'.$value.'" alt="Bootstrap" width="40" height="40">' . '</td>';
                    }
                    else{
                        $body .=  '<td>' . '<img src="'.$value.'" alt="Bootstrap" width="40" height="40">' . '</td>';
                    }
                }
                elseif($key =='role'){
                    if($user->CheckBan($row->id)){
                        $body .=  ' <td><a class="btn btn-primary bg-gradient" href="index.php?view=api/user/ManageBan/Unban/'.$row->id.'" role="button">Unban</a> </td>';
                    }
                    else{
                        $body .=  ' <td><a class="btn btn-danger bg-gradient" href="index.php?view=api/user/ManageBan/Ban/'.$row->id.'" role="button">Bannir</a> </td>';
                    }

                }
                else{
                    if($key=='countryid'){
                        $value = $menu->getCountryName( $value);
                    }

                    if($key == 'lastlogin'){
                        if(empty($value)){
                            $value = ' - ';
                        }
                    }
                    if ($value) {
                        $body .= '<td>' . $value . '</td>';
                    }
                }
            }
            $body .= '</tr>';
        }

        $result ='
          <div class = "espace2"></div>
          <section class="row nopadding bg-custom2">
              <div class="col-md-1"></div>
                  <div class="col pt-5 pb-5">
                        <div class="card " >
                              <div class="card-body"> 
                                    <h2 class="text-dark font pb-4">Liste des utilisateurs</h2>                                                      
                                    <table class="table table-striped table-dt" id="users-list">
                                        <thead>
                                            <tr>
                                                    <th>id</th>
                                                    <th></th>
                                                    <th>Username</th>
                                                    <th>Nom de famille</th>
                                                    <th>Prénom</th>
                                                    <th>Email</th>
                                                    <th>Pays</th>
                                                    <th>Date de création</th>
                                                    <th>Dernière connexion</th> 
                                                    <th>role</th>                                                             
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ' . $body . '
                                        </tbody>
                                    </table>                              
                              </div>
                        </div>      
                  </div>   
              <div class="col-md-1"></div>
          </section>
          <div class = "espace2"></div>
        ';

        return $result;
    }

    /**
     * @param array $data
     * @return string
     */
    public static function studentCourseList(array $data) : string {

        $body = '';
        $menu = new menu();
        $user = new user();

        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {

                    if ($value) {
                        $body .= '<td>' . $value . '</td>';
                    }
                }
            $body .= '<td><a class="btn btn-success bg-gradient" href="index.php?view=api/course/courseInscription/'.$row->fk_courseName.'/'.$row->year.'" role="button">S\'inscrire</a> </td>';
            }
            $body .= '</tr>';

        $result ='
          <section class="row nopadding ">
              <div class="col-md-2"></div>
                  <div class="col-md-9 pt-5 pb-5">
                        <div class="card " >
                              <div class="card-body">                                                     
                                    <h2 class="text-dark font pb-4">Liste des cours disponibles</h2>
                                    <table class="table table-striped table-dt" id="users-list">
                                        <thead>
                                            <tr>
                                                    <th>Cours</th>
                                                    <th>Branche</th>
                                                    <th>Année</th>
                                                    <th>Inscription</th>     
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ' . $body . '
                                        </tbody>
                                    </table>                              
                              </div>
                        </div>      
                  </div>   
              <div class="col-md-1"></div>
          </section>
        ';
        return $result;
    }

    /**
     * @param array $data
     * @return string
     */
    public static function studentCurentCourseList(array $data) : string{

        $body = '';
        $menu = new menu();
        $user = new user();

        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {

                if($key == 'note'){
                    if(empty ($value)){
                        $value = 'Aucune note pour le moment';
                    }
                }

                if($key == 'finished'){
                    if(empty ($value)){
                        $value = 'En cours';
                    } elseif($value == 1){
                        $value = 'Cours terminé';
                    }
                }

                if ($value) {
                    $body .= '<td>' . $value . '</td>';
                }
            }

        }
        $body .= '</tr>';

        $result ='
          <section class="row nopadding bg-custom2">
              <div class="col-md-2"></div>
                  <div class="col-md-9 pt-5 pb-5">
                        <div class="card " >
                              <div class="card-body">                                                     
                                    <h2 class="text-dark font pb-4">Vos cours</h2>
                                    <table class="table table-striped table-dt" id="users-list">
                                        <thead>
                                            <tr>
                                                    <th>Cours</th>
                                                    <th>Branche</th>
                                                    <th>Année</th>
                                                    <th>Note(sur 20)</th>   
                                                    <th>Etat</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ' . $body . '
                                        </tbody>
                                    </table>                              
                              </div>
                        </div>      
                  </div>   
              <div class="col-md-1"></div>
          </section>
        ';
        return $result;

    }

    /**
     * @param array $data
     * @return string
     */
    public static function awaitingStudentList(array $data): string {

        $body = '';
        $menu = new menu();
        $user = new user();

        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {
                if ($value) {
                    $body .= '<td>' . $value . '</td>';
                }

            }
            $body .= '<td><a class="btn btn-success bg-gradient" href="index.php?view=api/course/manageStudent/'.$row->id.'/accept/'.$row->nom.'" role="button">Accept</a> </td>';
            $body .= '<td><a class="btn btn-danger bg-gradient" href="index.php?view=api/course/manageStudent/'.$row->id.'/reject/'.$row->nom.'" role="button">Reject</a> </td>';
            $body .= '</tr>';
        }

        $result ='
          
          <section class="row nopadding">
              <div class="col-md-1"></div>
                  <div class="col pt-5 pb-5">
                        <div class="card " >
                              <div class="card-body"> 
                                    <h2 class="text-dark font pb-4">Liste des étudiants en attente</h2>                                                     
                                    <table class="table table-striped table-dt" id="users-list">
                                        <thead>
                                            <tr>
                                                    <th>Id</th>
                                                    <th>Nom de famille</th>
                                                    <th>Prénom</th>
                                                    <th>Username</th>
                                                    <th>Groupe</th>    
                                                    <th>Formation</th>          
                                                    <th></th>     
                                                    <th></th>                                                                                  
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ' . $body . '
                                        </tbody>
                                    </table>                              
                              </div>
                        </div>      
                  </div>   
              <div class="col-md-1"></div>
          </section>
        ';

        return $result;



    }

    /**
     * @param array $data
     * @param string $formationTitle
     * @return string
     */
    public static function PotentialTeacherList(array $data, string $formationTitle) : string {

        $body = '';

        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {

                if ($value) {
                    $body .= '<td>' . $value . '</td>';
                }
            }
            $body .='<td><a class="btn btn-success bg-gradient" href="index.php?view=api/course/AddTeacherInFormation/'.$row->id.'/'.$formationTitle.'" role="button">Ajouter</a> </td>';
            $body .= '</tr>';

        }

        $result ='
          
                                                   
                                    <table class="table table-striped table-dt" id="teacher-formation">
                                        <thead>
                                            <tr>
                                                 <th>Id</th>
                                                 <th>Username</th>
                                                 <th>Nom de famille</th>
                                                 <th>Prénom</th>  
                                                 <th>Action</th>    
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ' . $body . '
                                        </tbody>
                                    </table>                              

        ';
        return $result;
    }

    /**
     * @param array $data
     * @return string
     */
    public static function TeacherAvailableCourseList(array $data) : string {

        $body = '';
        $menu = new menu();
        $user = new user();

        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {

                if ($value) {
                    $body .= '<td>' . $value . '</td>';
                }
            }
            $body .= '<td><a class="btn btn-success bg-gradient" href="index.php?view=api/course/TeacherCourseInscription/'.$row->id.'" role="button">S\'inscrire</a> </td>';
        }
        $body .= '</tr>';

        $result ='
          <section class="row nopadding ">
              <div class="col-md-2"></div>
                  <div class="col-md-9 pt-5 pb-5">
                        <div class="card " >
                              <div class="card-body">                                                     
                                    <table class="table table-striped table-dt" id="users-list">
                                        <thead>
                                            <tr>
                                                    <th>Id</th>
                                                    <th>Branche</th>
                                                    <th>Cours</th>
                                                    <th>Année</th>   
                                                    <th>Inscription</th>    
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ' . $body . '
                                        </tbody>
                                    </table>                              
                              </div>
                        </div>      
                  </div>   
              <div class="col-md-1"></div>
          </section>
        ';
        return $result;
    }

    /**
     * @param array $data
     * @return string
     */
    public static function formationList(array $data):string{

        $user = new user();
        $course = new course();
        $admin= $user->SelectByField($_SESSION['userid'],'admin');

        // Getting awaiting formation from user and Transform into string array
        $AwaitingFormation = $course->GetStudentAwaitingOrAcceptedApplication($_SESSION['userid']);
        $AwaitingFormation = array_map(function ($object) { return $object->formationTitle; }, $AwaitingFormation);

        // Getting  already accepted formation from user where and Transform into string array
        $AlreadyAcceptedFormation = $course->GetStudentAwaitingOrAcceptedApplication($_SESSION['userid'],1);
        $AlreadyAcceptedFormation = array_map(function ($object) { return $object->formationTitle; }, $AlreadyAcceptedFormation);

        // Getting   formation from user where he got rejected and Transform into string array
        $RejectedFormation = $course->GetStudentRejectedApplication($_SESSION['userid']);
        $RejectedFormation = array_map(function ($object) { return $object->formationTitle; }, $RejectedFormation);

        $body='';
        foreach ($data as $row) {


            $body.='
                <div class="espace"></div> 
                        <div class="">
                            <div class="container px-0">
                                <section class="row nopadding">
                                    <div class="col-md-3 nopadding fill">                         
                                        ';
            if(empty($row->image)){
                $body.=                 '<img src="image\Course\Default.jpg" alt="Bootstrap" class ="pb-3 img-fluid" >';
            }
            else{
                $body.=                 '<img src="'.$row->image.'" alt="Bootstrap" class ="pb-3 img-fluid " >';
            }
            $body.='
                                    </div>
                                   <div class="col-md-1"></div>
                                   <div class="col-md-8">
                                        <div class="card cardSizing ">
                                            <div class="card-body">
                                                <h4 class="text-center font ">'.$row->formationTitle.'</h4>
                                                
                                                <h3 class="font pt-4">Description :</h3>
                                                <p class="pt-4 font">'.$row->desc.'</p>
                                                <p class="pt-4 font"> Durée : '.$row->time.' année</p>
                                                <p class="font"> Titre : '.$row->Level.'</p>';

            if(in_array($row->formationTitle,$RejectedFormation)){
                $body.='                        <a class="btn btn-danger bg-gradient font" href=""'.$row->formationTitle.'" role="button">Rejected</a>';
            }

            elseif(in_array($row->formationTitle,$AwaitingFormation)){
                $body.='                        <a class="btn btn-warning bg-gradient font" href=""'.$row->formationTitle.'" role="button">En attente</a>';
            }
            elseif(in_array($row->formationTitle,$AlreadyAcceptedFormation)){
                $body.='                        <a class="btn btn-secondary bg-gradient font" href="" role="button">Déja inscrit</a>';
            }
            else{
                $body.='                        <a class="btn btn-primary bg-gradient font" href="index.php?view=api/course/inscription/'.$row->formationTitle.'" role="button">Inscription</a>';
            }

                $body.='                                                                                
                                                <a class="btn btn-primary bg-gradient font courseList-modal-link link modal-formation" href="" role="button" data-bs-toggle="modal" data-bs-target="#modal-formation">Liste des cours</a>                                              
                                                <a class="btn btn-primary bg-gradient font pe-4" href="api/route/course/exportCourse/'.$row->formationTitle.'" role="button">Exporter</a> ';

            if(!empty($admin)){
                $body.=                         '<a class="btn btn-warning bg-gradient font courseList-teacher-modal-link link modal-formation-teacher" href="" role="button" data-bs-toggle="modal" data-bs-target="#modal-formation-teacher">Add Teacher</a>
                                                <a class="btn btn-warning bg-gradient font pe-4" href="index.php?view=api/course/removeFormation/'.urlencode($row->formationTitle).'" role="button">Remove</a>';
            }
            $body.='                        </div>
                                        </div>
                                   </div>           
                                </section>
                            </div>
                        </div>   
                    <div class="espace"></div>
                    ';
        }

        $body.='         <div class="modal fade modal-xl"  id="modal-formation" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                                  <div class="modal-body" id="modal-course-list-body">                                  
                                  </div>
                            </div>
                          </div>
                        </div>
            ';
        if(!empty($admin)){
            $body.='     <div class="modal fade modal-xl"  id="modal-formation-teacher" aria-hidden="true">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                            <div class="modal-body" id="modal-formation-teacher-body">                                  
                                          </div>
                                    </div>
                              </div>
                        </div>
            ';
        }
       return $body;
    }

    /**
     * @param array $data
     * @return string
     */
    public static function CourseListModal(array $data): string{

        $body = '';

        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {

                    if ($value) {
                        $body .= '<td>' . $value . '</td>';
                    }
            }
            $body .= '</tr>';
        }

        $result ='
          
                                                   
                                    <table class="table table-striped table-dt" id="course-list-formation">
                                        <thead>
                                            <tr>
                                                 <th>Cours :</th>
                                                 <th>Groupe de formation :</th>
                                                            
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ' . $body . '
                                        </tbody>
                                    </table>                              

        ';
        return $result;
    }

    /**
     * @param array $data
     * @return string
     */
    public static function TeacherCourseList(array $data): string {

            $body='';
            foreach ($data as $row) {

                $body.='
                <div class="espace2"></div> 
                        <div class="bg-custom">
                            <div class="container px-0">
                                <section class="row nopadding">
                                   <div class="col-md-2"></div>
                                   <div class="col-md-9">
                                        <div class="card cardSizing ">
                                            <div class="card-body">
                                                <h4 class="text-center font "></h4>                                              
                                                <div class = "row ">
                                                    <div class = "col-md-5 ">
                                                        <h3 class="font pt-4">'.$row->fk_courseName.'('.$row->year.')</h3>
                                                    </div>
                                                    <div class = "col-md-4"></div>
                                                    <div class = "col-md-3 pt-4">
                                                         <a class="btn btn-warning bg-gradient font" href="index.php?view=api/course/EndCourse/'.$row->id.'" role="button">End Course</a>
                                                         <a class="btn btn-danger bg-gradient font" href="index.php?view=api/course/TeacherRejectCourse/'.$row->id.'" role="button">Reject</a>                                                      
                                                    </div>                                             
                                                </div>                                               
                                                <h5 class="pt-4 font pb-2">'.$row->FK_branchename.'</h5>
                                                <h6 class="pt-1 font pb-2">'.$row->id.'</h6>
                                                <a class="btn btn-primary bg-gradient font studentListFromCourse-link" href="" role="button" data-bs-toggle="modal" data-bs-target="#studentListFromCourse">Student</a>
                                            </div>
                                        </div>
                                   </div>           
                                </section>
                            </div>
                        </div>   
                <div class="espace2"></div>';
            }
        $body.='<div class="modal fade modal-xl"  id="studentListFromCourse" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                                  <div class="modal-body" id="studentListFromCourse-body">                                  
                                  </div>
                            </div>
                          </div>
                        </div>       
        ';

            return $body;
        }

    /**
     * @param array $data
     * @return string
     */
    public static function TeacherCourseListModal(array $data): string {
        $body = '';

        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {

                if($key == 'note' AND empty($value)){
                    $value = "Aucune note";
                }
                if($key =="changenote"){
                    $value = '<a class="" href="index.php?view=api/menu/galery">Modifier note</a>';
                }


                if ($value) {
                    $body .= '<td>' . $value . '</td>';
                }
            }
            $body .= '
                        <td> 
                            <form action="index.php?view=api/course/ModifyStudentNote/'.$row->fk_yearly_courseID.'/'.$row->id.'" method="post" class="" enctype="multipart/form-data" >                       
                            <label for="StudentNote" class ="font"></label>
                            <input type="number" id="StudentNote" name="StudentNote" class="widthformLittle" required>                             
                            <button type="submit" class="btn btn-success font btn-sm bg-gradient ">Modifier</button>                    
                            </form>
                        </td>                       
                     
                     ';

            $body .= '</tr>';
        }

        $result ='
          
                                                   
                                    <table class="table table-striped table-dt" id="Student-list-from-course">
                                        <thead>
                                            <tr>
                                                 <th>Course Id</th>
                                                 <th>User Id</th>
                                                 <th>Nom de famille</th>
                                                 <th>Prénom</th>     
                                                 <th>Note</th>     
                                                 <th></th>  
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ' . $body . '
                                        </tbody>
                                    </table>                              

        ';
        return $result;
    }

    /**
     * @param array $data
     * @return string
     */
    public static function AdminTeacherList(array $data):string{
        $body = '';

        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {

                    if($key == 'rejected'){
                        if($value == 1){
                            $value = '<a class="btn btn-primary bg-gradient font" href="index.php?view=api/course/AdminRejectTeacher/'.$row->id.'/'.$row->formationTitle.'/add" role="button">Rajouter</a>';
                        }else{
                            $value = '<a class="btn btn-danger bg-gradient font" href="index.php?view=api/course/AdminRejectTeacher/'.$row->id.'/'.$row->formationTitle.'/remove" role="button">Remove</a>';
                        }
                    }
                    if ($value) {
                        $body .= '<td>' . $value . '</td>';
                    }
            }
            $body .= '</tr>';
        }

        $result ='
          <div class = "espace2"></div>
          <section class="row nopadding bg-custom2">
              <div class="col-md-1"></div>
                  <div class="col pt-5 pb-5">
                        <div class="card " >
                              <div class="card-body"> 
                                    <h2 class="text-dark font pb-4">Liste des enseignants</h2>                                                    
                                    <table class="table table-striped table-dt" id="users-list">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nom de famille</th>
                                                <th>Prénom</th>
                                                <th>Section</th>
                                                <th></th>
                                                                                                      
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ' . $body . '
                                        </tbody>
                                    </table>                              
                              </div>
                        </div>      
                  </div>   
              <div class="col-md-1"></div>
          </section>
          <div class = "espace2"></div>
        ';

        return $result;
    }

    /**
     * @param array $data
     * @return string
     */
    public static function AdminStudentList(array $data):string{
        $body = '';
        $course = new course();
        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {

                if($key == 'removed'){
                    if($value == 1){
                        $value = '<a class="btn btn-primary bg-gradient font" href="index.php?view=api/course/AdminRejectStudent/'.$row->id.'/'.$row->nom.'/add" role="button">Rajouter</a>';
                    }else{
                        $value = '<a class="btn btn-danger bg-gradient font" href="index.php?view=api/course/AdminRejectStudent/'.$row->id.'/'.$row->nom.'/remove" role="button">Remove</a>';
                    }
                }
                if($key == 'note'){
                    $value ='<a class="btn btn-primary bg-gradient font studentNoteListFromFormation-link" href="" role="button" data-bs-toggle="modal" data-bs-target="#studentNoteListFromFormation">Note</a>';
                }
                if($key == 'validate'){
                    $check = $course->GetNextBranche($row->nom);
                    if(empty($check)){
                        $value ='<a class="btn btn-secondary bg-gradient font" href="" role="button">Finished formation</a>';
                    } else {
                        $value ='<a class="btn btn-primary bg-gradient font" href="index.php?view=api/course/AdminUpgradeStudent/'.$row->id.'/'.$check.'/'.$row->nom.'" role="button">Validate year</a>';
                    }
                }
                if ($value) {
                    $body .= '<td>' . $value . '</td>';
                }
            }
            $body .= '</tr>';
        }
        $result ='
          <div class = "espace2"></div>
          <section class="row nopadding bg-custom2">
              <div class="col-md-1"></div>
                  <div class="col pt-5 pb-5">
                        <div class="card " >
                              <div class="card-body">                                                     
                                    <h2 class="text-dark font pb-4">Liste des étudiants</h2>
                                    <table class="table table-striped table-dt" id="users-list">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Branche</th>
                                                <th>Formation</th> 
                                                <th></th>
                                                <th>Note</th>
                                                <th>Nom de famille</th>
                                                <th>Prénom</th>
                                                <th></th> 
                                                                                                     
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ' . $body . '
                                        </tbody>
                                    </table>                              
                              </div>
                        </div>      
                  </div>   
              <div class="col-md-1"></div>
          </section>
          <div class = "espace2"></div>
        ';
        $result.='<div class="modal fade modal-xl"  id="studentNoteListFromFormation" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                                  <div class="modal-body" id="studentNoteListFromFormation-body">                                  
                                  </div>
                            </div>
                          </div>
                        </div>       
        ';
        return $result;
    }

    /**
     * @param array $data
     * @return string
     */
    public static function AdminStudentList_Coursemodal(array $data):string{

        $body = '';

        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {

                if($key == 'note'){
                    if(empty($value) OR $value == 'null'){
                        $value = 'Aucune note';
                    }
                }

                if($key == 'finished'){
                    if(empty($value) OR $value == 'null'){
                        $value = 'En cours';
                    }else{
                        $value = 'Terminé';
                    }
                }

                if ($value) {
                    $body .= '<td>' . $value . '</td>';
                }
            }
            $body .= '</tr>';
        }

        $result ='
          
                                                   
                                    <table class="table table-striped table-dt" id="Student-course-from-branche">
                                        <thead>
                                            <tr>
                                                 <th>Cours</th>
                                                 <th>Année</th>
                                                 <th>Note</th>
                                                 <th>Status</th>       
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ' . $body . '
                                        </tbody>
                                    </table>                              
        ';
        return $result;

    }

    /**
     * @param array $data
     * @return string
     */
    public static function AdminFormationDashboard(array $data):string{
        $body = '';
        $course = new course();
        $year = $course->getCurrentYear();
        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {

                if ($value) {
                    $body .= '<td>' . $value . '</td>';
                }
            }
            $body.= '<td><a class="btn btn-success bg-gradient font" href="index.php?view=api/course/reAddFormation/'.urlencode($row->formationTitle).'" role="button">Rajouter</a></td>';
            $body .= '</tr>';
        }
        $result ='
                  <div class = "espace2"></div>
                  <section class="row nopadding bg-custom2">
                        <div class="col-md-1"></div>
                            <div class="col pt-5 pb-5">
                                <div class="card " >
                                        <div class="card-body text-dark">                                                     
                                                <h2 class="text-dark font pb-4">Modifier année scolaire</h2>                                                     
                                                <form action = "index.php?view=api/year/ModifyCurrentYear" method="post" enctype="multipart/form-data" autocomplete="off" >                                                                                                                                     
                                                     <div class = "pb-2">
                                                            <label for="newyear" class="widthform">Nouvelle année</label>                                                      
                                                            <input type="number" id="newyear" name="newyear" class="widthform" required  value="'.$year.'">
                                                     </div>  
                                                     
                                                     <br>                                                    
                                                     <div class = "pb-1">                                                             
                                                     <label for="actif" class ="font widthform">Rendre active</label>
                                                            <select name="actif" id="actif" class="widthform">
                                                                <option value="yes">Yes</option>
                                                            </select>
                                                     </div>                                    
                                                     <div class ="pt-2 pe-4">
                                                            <button type="submit" class=" btn border-0 btn-primary font btn-lg bg-gradient">Modifier</button>                                                    
                                                     </div>                                               
                                                </form>                                    
                                        </div>
                                </div>      
                            </div>   
                        <div class="col-md-1"></div>
                  </section>
                  
                  
                  <section class="row nopadding bg-custom2">
                      <div class="col-md-1"></div>
                          <div class="col pt-5 pb-5">
                                <div class="card " >
                                      <div class="card-body">                                                     
                                            <h2 class="text-dark font pb-4">Formation inactive</h2>
                                            <table class="table table-striped table-dt" id="users-list">
                                                <thead>
                                                    <tr>
                                                        <th>Formation</th>
                                                        <th>Niveau</th>
                                                        <th></th>                                                                                                              
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    ' . $body . '
                                                </tbody>
                                            </table>                              
                                      </div>
                                </div>      
                          </div>   
                      <div class="col-md-1"></div>
                  </section>
                  
                  <section class="row nopadding bg-custom2">
                        <div class="col-md-1"></div>
                            <div class="col pt-5 pb-5">
                                <div class="card " >
                                        <div class="card-body text-dark">                                                     
                                                <h2 class="text-dark font pb-4">Ajouter Formation</h2>
                                                        
                                                <form action = "index.php?view=api/course/AddFormation" method="post" enctype="multipart/form-data" autocomplete="off" >                                                                                 
                                                     <div class = "pb-2">
                                                            <label for="formationTitle" class="widthform">Nom de formation</label>                                                      
                                                            <input type="text" id="formationTitle" name="formationTitle" class="widthform" required>
                                                     </div>  
                                                         <br> 
                                                     <div class = "pb-1">                                                             
                                                            <label for="Level" class="widthform">Niveau de formation</label>                                                      
                                                            <input type="text" id="Level" name="Level" class="widthform" required>
                                                     </div>
                                                         <br>
                                                     <div class = "pb-4">                                                             
                                                            <label for="time" class="widthform">Durée (Année)</label>                                                      
                                                            <input type="number" id="time" name="time" class="widthform" required>
                                                     </div>
                                                         
                                                     <div class = "pb-4">                                                             
                                                            <label for="desc" class="widthform">Description (Court)</label>                                                      
                                                            <input type="text" id="desc" name="desc" class="widthform" required>
                                                     </div>
                                                            
                                                     <label for="firstbranche" class="widthform">Nom de la première année(Court)</label>                                                      
                                                            <input type="text" id="firstbranche" name="firstbranche" class="widthform" required>
                                                     </div>                                        
                                                     <div class ="pt-2 pe-4">
                                                            <button type="submit" class=" btn border-0 btn-primary font btn-lg bg-gradient">Modifier</button>
                                                     </div>
                                                </form>                                           
                                        </div>
                                </div>      
                            </div>   
                        <div class="col-md-1"></div>
                  </section>
                  

                ';
        return $result;
    }

    /**
     * @return string
     */
    public static function navbar(): string
    {
       if(!empty($_SESSION['userid'])){
           $course = new course();
           $user = new user();
           $image= $user->SelectByField($_SESSION['userid'],'image');
           $admin= $user->SelectByField($_SESSION['userid'],'admin');
           $teacher=$course->checkTeacherRole($_SESSION['userid']);
           $student =$course->checkStudentRole($_SESSION['userid']);
       }


        $body='
            <nav class="navbar navbar-expand-lg navbar-dark bg-gradient bg-custom">
                <div class="container-fluid blured">
                    <!-- BRAND -->
                    <a class="navbar-brand" href="#">
                <img class="d-inline-block align-top " src="image/Navbar/Logo.png" width="120" height = "70" alt="">
                    </a>
                    <!-- BOUTON BURGER -->
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!-- ITEM DE LA NAVBAR -->
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
                            <!-- Acceuil -->
                            <li class="nav-item">
                                <a class="nav-link  font" aria-current="page" href="index.php?view=api/menu/home">Acceuil</a>
                            </li>
                            <!-- GALERIE -->
                            <li class="nav-item">
                                <a class="nav-link font" href="index.php?view=api/menu/galery">Galerie</a>
                            </li>';

        if(!empty($_SESSION['userid'])){
            $body .='    
                            <!-- Courses -->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle font" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Cours</a>
                                <ul class="dropdown-menu dropdown-menu-dark">
                                    <li><a class="dropdown-item" href="index.php?view=api/course/cursus">Cursus scolaire</a></li>';
            if(!empty($student)) {

                $body.='       
                                    <li><a class="dropdown-item" href="index.php?view=api/course/availaibleCourse">Inscription cours</a></li>
                                    <li><a class="dropdown-item" href="index.php?view=api/course/myCourse">Mes cours</a></li> ';
            }
            $body.=' 
                                </ul>
                            </li>  ';

            if(!empty($teacher)){
                $body .='    
                            <!-- Courses -->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle font" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Teacher</a>
                                <ul class="dropdown-menu dropdown-menu-dark">
                                    <li><a class="dropdown-item" href="index.php?view=api/course/teacherAvailaibleCourseInscription">Inscription cours</a></li>
                                    <li><a class="dropdown-item" href="index.php?view=api/course/TeacherMyCourse">Mes cours</a></li>
                                </ul>
                            </li>              ';
            }

            if(!empty($admin)){
                $body.='
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle font" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Admin</a>
                                <ul class="dropdown-menu dropdown-menu-dark">
                                    <li><a class="dropdown-item" href="index.php?view=api/user/userList">Liste - User</a></li>
                                    <li><a class="dropdown-item" href="index.php?view=api/course/AdminStudentList">Liste - Student</a></li>
                                    <li><a class="dropdown-item" href="index.php?view=api/course/AdminTeacherList">Liste - Teacher</a></li>
                                    <li><a class="dropdown-item" href="index.php?view=api/course/awaitingStudent">Inscription - Student</a></li>
  
                                </ul>
                            </li> 
                            <li class="nav-item">
                                <a class="nav-link font" href="index.php?view=api/course/AdminFormationDashboard">Formation(Admin)</a>
                            </li>
                ';
            }

        }
        $body .='
                            <!-- EVENEMENT -->

                        </ul>
                        <!-- CONNEXION AVEC DROPDOWN DROITE -->
                        <ul class="navbar-nav">';

        if(empty($_SESSION['userid'])){
            $body .='                    
                            <li class="nav-item dropdown navdropright">
                                <a class="nav-link dropdown-toggle btn btn-dark font"  href="#" id="navbarDropdown"  role="button" data-bs-toggle="dropdown" aria-expanded="false">Connexion</a>
                                <ul class="dropdown-menu " aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item link modal-login font" href="#" data-bs-toggle="modal" data-bs-target="#modal-login">Se connecter</a></li>
                                    <li><a class="dropdown-item link modal-create font" href="#" data-bs-toggle="modal" data-bs-target="#modal-create">Créer un compte</a></li>
                                </ul>
        
                            </li>';
        }
        else{
            $body .='
                              <li class="nav-item dropdown navdropright">
                                  <div class="container">
                                        <a class=" nav-link dropdown-toggle" href="#" id="navbarDropdown"  role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                        
            if(!empty($image) AND $image != 'NULL'){
                $body .='<img src="'.$image.'" alt="Bootstrap" class = "rounded" width="40" height="40">';
            }else{
                $body .='<img src="image/user/photo/Default.png" alt="Bootstrap" width="40" height="40">';
            }


            $body .='    </a>
                                        <ul class="dropdown-menu " aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item link font" href="index.php?view=api/user/profil" >Profil</a></li>
                                            <li><a class="dropdown-item link font" href="index.php?view=api/user/logout" >Se déconnecter</a></li>
                                        </ul>
                                  </div>
                            </li>';
        }
        $body .=' 
                        </ul>
                    </div>
                </div>
            </nav>
    <!-- LOGIN MODAL -->
            <div class="modal fade " tabindex="-1" id="modal-login" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content ">
                        <div class="">
                            <section class="row nopadding">
                                <div class="col-md-5  p-4 bg-custom4 nopadding">
                                    <img src="image/Modal/modal-1.png" alt="" class="img-fluid w-100">
                                </div>
                                <div class="col-md-7 ps-5 bg-custom2">
                                    <div class="container-contact nopadding ">
                                        <div class="pb-4 ">
                                            <h4 class="pb-3 display-6 pt-4 font ">Log in</h4>
                                            <div class = "pb-5">
                                                <form action = "index.php?view=api/user/login" method="post" enctype="multipart/form-data" >
                                                    <label for="email" class ="font" >E-mail</label>
                                                    <input type="email" id="email" name="email" class="widthform">
                                                    <br>
                                                    <label for="password" class="font">Mot de passe</label>
                                                    <input type="password" id="password" name="password" class="widthform">
                                                    <br>
                                                    <div class ="pt-5">
                                                        <button type="submit" class=" btn border-0 btn-custom5 font btn-lg bg-gradient">Login</button>
                                                    </div>
                                                </form>
                                            </div>
            
                                        </div>
                                    </div>
                                </div>
            
                            </section>
                        </div>
            
                    </div>
                </div>
            </div>
            <!-- CREATE MODAL -->
            <div class="modal fade" tabindex="-1" id="modal-create" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl">
                    <div class="modal-content ">
                        <div class="">
                            <section class="row nopadding">
                                <div class="col-md-4  p-4 bg-custom4  nopadding">
                                    <img src="image/Modal/modal-4.png" alt="" class="img-fluid w-100">               
                                </div>
                                <div class="col-md-8 ps-5 bg-custom2">
                                    <div class="container-contact nopadding ">
                                        <div class="pb-4 ">
                                            <h4 class="pb-3 display-7 pt-4 font font-35px pb-4">Création de votre compte</h4>
                                            <div class = "pb-5">
                                                <form action="index.php?view=api/user/create" method="post" class="" enctype="multipart/form-data" >
                                                    <label for="last-name" class ="font">Nom de famille</label>
                                                    <input type="text" id="last-name" name="last-name" class="widthform" maxlength="20" required>
            
                                                    <label for="first-name" class="font">Prénom</label>
                                                    <input type="text" id="first-name" name="first-name" class="widthform" maxlength="20" required>
            
                                                    <label for="username" class = "font">Identifiant</label>
                                                    <input type="text" id="username" name="username" class="widthform" maxlength="20" required>
            
                                                    <label for="email" class ="font" >E-mail</label>
                                                    <input type="email" id="email" name="email" class="widthform" maxlength="35" required>
            
                                                    <label for="Password" class="font">Mot de passe</label>
                                                    <input type="password" id="password" name="password" class="widthform" required>
            
                                                    <label for="passverify" class="font">Confirmer mot de passe</label>
                                                    <input type="password" id="passverify" name="passverify" class="widthform" required>
            
                                                    <label for="Adresse" class="font">Adresse</label>
                                                    <input type="text" id="Adresse" name="Adresse" class="widthform" required>
            
                                                    <label for="country" class ="font">Pays</label>
                                                    <select name="country" id="country" class="widthform">
                                                        <option value="be">Belgique</option>
                                                        <option value="fr">France</option>
                                                        <option value="al">Allemagne</option>
                                                        <option value="ru">Russie</option>
                                                        <option value="ot">Autre</option>
                                                    </select>
                                                    <label for="lang" class ="font">Langue</label>
                                                    <select name="lang" id="lang" class="widthform">
                                                        <option value="fr">Francais</option>
                                                        <option value="en">Anglais</option>
                                                     </select>
                                                    <label for="phone" class="font">Numéro de téléphone</label>
                                                    <input type="tel" id="phone" name="phone" class="widthform" maxlength="9" required>
                                                    <div class="mb-2 pt-2 form-check">
                                                        <label class="form-check-label" for="news">Inscription Newsletter</label>
                                                        <input type="checkbox" class="form-check-input nowidth" name ="news" id="news" checked = "checked">
                                                    </div>
                                                    <div class="pt-3 nopadding">
                                                        <button type="submit" class=" widthform2 btn btn-custom5 font btn-lg bg-gradient ">Créer le compte</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>';
        return $body;
    }

    /**
     * @return string
     */
    public static function carousel(): string
    {
        $body="";
        $body=file_get_contents(ROOT_PATH .'/view/component/carousel.php');
        return $body;
    }
}