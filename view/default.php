
<?php

use app\Helpers\Output;
use app\Controllers\menu;
use app\Helpers\Text;



if (!empty($_GET['view']) && $_GET['view'] != 'view/default') {
    // Routing
    try {
        Output::getContent($_GET['view']);
    } catch (Exception $e) {
        Output::render('messageBox', 'Veuillez utilisez l\'interface du site uniquement');
    }
} else {

    output::staticRender('navbar');
    output::staticRender('carousel');
    output::staticRender('homemenu');
}

