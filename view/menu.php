
<nav class="navbar navbar-expand-lg navbar-dark bg-gradient bg-custom">
    <div class="container-fluid blured">
        <!-- BRAND -->
        <a class="navbar-brand" href="#">
            <img class="d-inline-block align-top " src="image/Navbar/Logo.png" width="120" height = "70" alt="">
        </a>
        <!-- BOUTON BURGER -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- ITEM DE LA NAVBAR -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
                <!-- Acceuil -->
                <li class="nav-item">
                    <a class="nav-link active font" aria-current="page" href="index.php?view=api/menu/home">Acceuil</a>
                </li>
                <!-- GALERIE -->
                <li class="nav-item">
                    <a class="nav-link font" href="">Galerie</a>
                </li>
                <!-- EVENEMENT -->
                <li class="nav-item">
                    <a class="nav-link font" href="">Evénements</a>
                </li>
                <!-- A propos de nous -->
                <li class="nav-item">
                    <a class="nav-link font" href="">A propos de nous</a>
                </li>
            </ul>
            <!-- CONNEXION AVEC DROPDOWN DROITE -->
            <ul class="navbar-nav">
                <li class="nav-item dropdown navdropright">
                    <a class="nav-link dropdown-toggle btn btn-dark font"  href="#" id="navbarDropdown"  role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Connexion</a>
                    <ul class="dropdown-menu " aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item link modal-login font" href="#" data-bs-toggle="modal" data-bs-target="#modal-login">Se connecter</a></li>
                        <li><a class="dropdown-item link modal-create font" href="#" data-bs-toggle="modal" data-bs-target="#modal-create">Créer un compte</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- LOGIN MODAL -->
<div class="modal fade " tabindex="-1" id="modal-login" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content ">
            <div class="">
                <section class="row nopadding">
                    <div class="col-md-5  p-4 bg-custom4 nopadding">
                        <img src="image/Modal/modal-1.png" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-7 ps-5 bg-custom2">
                        <div class="container-contact nopadding ">
                            <div class="pb-4 ">
                                <h4 class="pb-3 display-6 pt-4 font ">Log in</h4>
                                <div class = "pb-5">
                                    <form action = "index.php?view=api/user/login" method="post" enctype="multipart/form-data" >
                                        <label for="email" class ="font" >E-mail</label>
                                        <input type="email" id="email" name="email" class="widthform">
                                        <br>
                                        <label for="password" class="font">Mot de passe</label>
                                        <input type="password" id="password" name="password" class="widthform">
                                        <br>
                                        <div class ="pt-5">
                                            <button type="submit" class=" btn border-0 btn-custom5 font btn-lg bg-gradient">Login</button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                </section>
            </div>

        </div>
    </div>
</div>
<!-- CREATE MODAL -->
<div class="modal fade" tabindex="-1" id="modal-create" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content ">
            <div class="">
                <section class="row nopadding">
                    <div class="col-md-4  p-4 bg-custom4  nopadding">
                        <img src="image/Modal/modal-4.png" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-8 ps-5 bg-custom2">
                        <div class="container-contact nopadding ">
                            <div class="pb-4 ">
                                <h4 class="pb-3 display-7 pt-4 font font-35px pb-4">Création de votre compte</h4>
                                <div class = "pb-5">
                                    <form action="index.php?view=api/user/create" method="post" class="" enctype="multipart/form-data" >
                                        <label for="last-name" class ="font">Nom de famille</label>
                                        <input type="text" id="last-name" name="last-name" class="widthform" required>

                                        <label for="first-name" class="font">Prénom</label>
                                        <input type="text" id="first-name" name="first-name" class="widthform" required>

                                        <label for="username" class = "font">Identifiant</label>
                                        <input type="text" id="username" name="username" class="widthform" required>

                                        <label for="email" class ="font" >E-mail</label>
                                        <input type="email" id="email" name="email" class="widthform" required>

                                        <label for="Password" class="font">Mot de passe</label>
                                        <input type="password" id="password" name="password" class="widthform" required>

                                        <label for="passverify" class="font">Confirmer mot de passe</label>
                                        <input type="password" id="passverify" name="passverify" class="widthform" required>

                                        <label for="Adresse" class="font">Adresse</label>
                                        <input type="text" id="Adresse" name="Adresse" class="widthform" required>

                                        <label for="country" class ="font">Pays</label>
                                        <select name="country" id="country" class="widthform">
                                            <option value="be">Belgique</option>
                                            <option value="fr">France</option>
                                            <option value="al">Allemagne</option>
                                            <option value="ru">Russie</option>
                                            <option value="others">Autre</option>
                                        </select>
                                        <label for="lang" class ="font">Langue</label>
                                        <select name="lang" id="lang" class="widthform">
                                            <option value="fr">Francais</option>
                                            <option value="en">Anglais</option>
                                        </select>
                                        <label for="phone" class="font">Numéro de téléphone</label>
                                        <input type="tel" id="phone" name="phone" class="widthform" required>
                                        <div class="mb-2 pt-2 form-check">
                                            <label class="form-check-label" for="news">Inscription Newsletter</label>
                                            <input type="checkbox" class="form-check-input nowidth" name ="news" id="news" checked = "checked">
                                        </div>
                                        <div class="pt-3 nopadding">
                                            <button type="submit" class=" widthform2 btn btn-custom5 font btn-lg bg-gradient ">Créer le compte</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>