

$(document).ready( function ($) {


    let dt = $('.table-dt, .table-dten');
    console.log(dt);
    dt.dataTable({
        "pageLength": 25
    });
    let tabledts = document.querySelectorAll('.table-dt, .table-dten');
    console.log(tabledts);

    let tabledt = document.querySelector('.table-dt, .table-dten');
    console.log(tabledt);


    $('.table-dtfr').DataTable( {
        language: {
            processing:     "Traitement en cours...",
            search:         "Rechercher&nbsp;:",
            lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
            info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "Aucune donnée disponible dans le tableau",
            paginate: {
                first:      "Premier",
                previous:   "Pr&eacute;c&eacute;dent",
                next:       "Suivant",
                last:       "Dernier"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        "pageLength": 25
    });

    //window.addEventListener("load", function(e) {


        //  MODAL GENERATION  CONTENT (Datatable) FOR COURSES LIST IN COURS -> Cursus scolaire -> "Liste de cours" BUTTON

        let modalBody = document.querySelector("#modal-course-list-body");
        let FormationModalLinks = document.querySelectorAll(".courseList-modal-link");

        FormationModalLinks.forEach((link) => {
            if (typeof(link) != 'undefined' && link != null) {
                link.addEventListener("click", function (e) {
                    e.preventDefault();

                    let Sib = link.previousElementSibling;
                    let Sib2 = Sib.previousElementSibling;
                    let Sib3 = Sib2.previousElementSibling;
                    let Sib4 = Sib3.previousElementSibling;
                    let Sib5 = Sib4.previousElementSibling;
                    let Sib6 = Sib5.previousElementSibling.textContent;

                    modalBody.innerHTML = '';

                    fetch("api/route/course/CourseListByFormation/" + Sib6 , {
                    }).then(function (response) {
                        return response.text();
                    }).then(function (response) {
                        modalBody.innerHTML = response.toString();
                        $('#course-list-formation').DataTable();
                    }).catch((error) => {
                        console.log(error.toString());
                    });
                });
            }
        });

        //  MODAL GENERATION CONTENT (Datatable) FOR Potential Teacher LIST IN COURS -> Cursus scolaire -> "Add Teacher" BUTTON

        let modalBodyTeacher = document.querySelector("#modal-formation-teacher-body");
        let FormationModaTeacherlLinks = document.querySelectorAll(".courseList-teacher-modal-link");

        FormationModaTeacherlLinks.forEach((link) => {
            if (typeof(link) != 'undefined' && link != null) {
                link.addEventListener("click", function (e) {
                    e.preventDefault();

                    let Sib = link.previousElementSibling;
                    let Sib2 = Sib.previousElementSibling;
                    let Sib3 = Sib2.previousElementSibling;
                    let Sib4 = Sib3.previousElementSibling;
                    let Sib5 = Sib4.previousElementSibling;
                    let Sib6 = Sib5.previousElementSibling;
                    let Sib7 = Sib6.previousElementSibling;
                    let Sib8 = Sib7.previousElementSibling.textContent;

                    modalBodyTeacher.innerHTML = '';

                    fetch("api/route/course/BrancheAddTeacher/" + Sib8 , {
                    }).then(function (response) {
                        return response.text();
                    }).then(function (response) {
                        modalBodyTeacher.innerHTML = response.toString();
                        $('#teacher-formation').DataTable();
                    }).catch((error) => {
                        console.log(error.toString());
                    });
                });
            }
        });

        //  MODAL GENERATION CONTENT (Datatable) FOR Student FROM Course in Teacher -> Mes cours -> Student button

        let modalBodyTeacherStudentList = document.querySelector("#studentListFromCourse-body");
        let TeacherCourseStudentList = document.querySelectorAll(".studentListFromCourse-link");

        TeacherCourseStudentList.forEach((link) => {
            if (typeof(link) != 'undefined' && link != null) {
                link.addEventListener("click", function (e) {
                    e.preventDefault();

                    let Sib = link.previousElementSibling.textContent;


                    modalBodyTeacherStudentList.innerHTML = '';

                    fetch("api/route/course/TeacherMyCourseModal/" + Sib , {
                    }).then(function (response) {
                        return response.text();
                    }).then(function (response) {
                        modalBodyTeacherStudentList.innerHTML = response.toString();
                        $('#Student-list-from-course').DataTable();
                    }).catch((error) => {
                        console.log(error.toString());
                    });
                });
            }
        });

        //  MODAL GENERATION CONTENT (Datatable) FOR Student Course information in Admin -> Liste-Student -> Note button

        let modalBodyStudentNoteList = document.querySelector("#studentNoteListFromFormation-body");
        let StudentNoteList = document.querySelectorAll(".studentNoteListFromFormation-link");

        StudentNoteList.forEach((link) => {
            if (typeof(link) != 'undefined' && link != null) {
                link.addEventListener("click", function (e) {
                    e.preventDefault();


                    let td = link.closest('td');
                    let td2 = td.previousSibling;
                    let td3 = td2.previousSibling;
                    let branchename = td3.previousSibling;
                    let userid = branchename.previousSibling;
                    modalBodyStudentNoteList.innerHTML = '';

                    fetch("api/route/course/AdminStudentListModal/"+userid.textContent+"/"+branchename.textContent, {
                    }).then(function (response) {
                        return response.text();
                    }).then(function (response) {
                        modalBodyStudentNoteList.innerHTML = response.toString();
                        $('#Student-course-from-branche').DataTable();
                    }).catch((error) => {
                        console.log(error.toString());
                    });
                });
            }
        });


    });
//} );