<?php

session_name('EXAM' . date('Ymd'));
session_start(['cookie_lifetime' => 3600]);

// auto load classes

spl_autoload_register(function ($class) {
    require __DIR__ . '/' . strtolower(str_replace('\\', DIRECTORY_SEPARATOR, $class)) . '.php';
});


$connect = \app\Helpers\DB::connect();

// output
require_once __DIR__ . '/view/header.html';
require_once __DIR__ . '/view/default.php';
require_once __DIR__ . '/view/footer.html';

