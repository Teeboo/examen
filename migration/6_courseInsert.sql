INSERT INTO formation () VALUES
    ('Informatique de gestion','Bachelier',1,'image\\Course\\Bacinfo.jpg','Le bachelier en informatique de gestion est un collaborateur polyvalent qui met en oeuvre la diversité méthodologique des différentes fonctions de l’informaticien en réponse aux besoins des organisations.',3),
    ('Webdeveloper','BES',1,'image\\Course\\WebDev.jpg','Le Web developer participe aux activités concernant la conception, la réalisation, la mise à jour, la maintenance et l''évaluation d''applications internet/intranet statiques et dynamiques.',2);

INSERT INTO branche (nom,formationTitle,prepreq,orderInt) VALUES
    ('BacInfo1','Informatique de gestion',NULL,1),
    ('BacInfo2','Informatique de gestion','BacInfo1',2),
    ('BacInfo3','Informatique de gestion','BacInfo2',3),
    ('BacInfo4','Informatique de gestion','BacInfo3',4),
    ('Webdev01','Webdeveloper',NULL,1),
    ('Webdev02','Webdeveloper','Webdev01',2);

INSERT INTO course (nom,FK_branchename) VALUES

    ('Initiation aux bases de données','BacInfo1'),
    ('Principes algorithmiques et programmation','BacInfo1'),
    ('Web : principes de base','BacInfo1'),
    ('Structure des ordinateurs	','BacInfo1'),
    ('Gestion et exploitation de bases de données','BacInfo1'),
    ('Techniques de gestion de projet','BacInfo1'),
    ('Principes d’analyse informatique','BacInfo1'),

    ('Bases des réseaux','BacInfo2'),
    ('Projet de développement web','BacInfo2'),
    ('Programmation orientée objet','BacInfo2'),
    ('Projet d’analyse et de conception','BacInfo2'),
    ('Système d’exploitation','BacInfo2'),

    ('Administration, gestion et sécurisation des réseaux','BacInfo3'),
    ('Projet de développement SGBD','BacInfo3'),
    ('Notions de E-business','BacInfo3'),
    ('Veille technologique','BacInfo3'),

    ('Information et communication professionnelle','BacInfo4'),
    ('Produits logiciels de gestion intégrés','BacInfo4'),
    ('Mathématiques appliquées à l’informatique','BacInfo4'),


    ('Anglais UE1','Webdev01'),
    ('Création de sites web statiques','Webdev01'),
    ('Environnement et technologies du web','Webdev01'),
    ('Base des réseaux','Webdev01'),
    ('Approche Design','Webdev01'),
    ('Initiation à la programmation','Webdev01'),

    ('Anglais UE2','Webdev02'),
    ('SGBD - Système de gestion de bases de données','Webdev02'),
    ('Projet Web dynamique','Webdev02'),
    ('Scripts serveurs','Webdev02'),
    ('Scripts clients','Webdev02'),
    ('Framework et POO côté Serveur','Webdev02');

INSERT INTO yearly_course (fk_courseName, year) VALUES



    ('Initiation aux bases de données',2022),
    ('Principes algorithmiques et programmation',2022),
    ('Web : principes de base',2022),
    ('Structure des ordinateurs	',2022),
    ('Gestion et exploitation de bases de données',2022),
    ('Techniques de gestion de projet',2022),
    ('Principes d’analyse informatique',2022),

    ('Bases des réseaux',2022),
    ('Projet de développement web',2022),
    ('Programmation orientée objet',2022),
    ('Projet d’analyse et de conception',2022),
    ('Système d’exploitation',2022),

    ('Administration, gestion et sécurisation des réseaux',2022),
    ('Projet de développement SGBD',2022),
    ('Notions de E-business',2022),
    ('Veille technologique',2022),

    ('Information et communication professionnelle',2022),
    ('Produits logiciels de gestion intégrés',2022),
    ('Mathématiques appliquées à l’informatique',2022),


    ('Anglais UE1',2022),
    ('Création de sites web statiques',2022),
    ('Environnement et technologies du web',2022),
    ('Base des réseaux',2022),
    ('Approche Design',2022),
    ('Initiation à la programmation',2022),

    ('Anglais UE2',2022),
    ('SGBD - Système de gestion de bases de données',2022),
    ('Projet Web dynamique',2022),
    ('Scripts serveurs',2022),
    ('Scripts clients',2022),
    ('Framework et POO côté Serveur',2022);


    INSERT INTO year VALUE (2022,1);

