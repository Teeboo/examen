CREATE TABLE IF NOT EXISTS `country`(
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `countryname` varchar(50) NOT NULL,
    `countryId` VARCHAR(5) NOT NULL,

    PRIMARY KEY (`id`),
    UNIQUE KEY (`countryId`)

    )ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `country` (countryId, countryname) VALUES
    ("be", 'Belgique'),
    ("fr", 'France'),
    ("al", 'Allemagne'),
    ("ru", 'Russie'),
    ("ot", 'Others');
