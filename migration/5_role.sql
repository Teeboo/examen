CREATE TABLE `t_ban` (

                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `userid` int(10) unsigned NOT NULL,
                         `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         PRIMARY KEY (`id`),
                         CONSTRAINT FK_userid1 FOREIGN KEY (userid) REFERENCES user(id),
                         UNIQUE KEY `userid` (`userid`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `t_guest` (

                           `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                           `userid` int(10) unsigned NOT NULL,
                           `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           PRIMARY KEY (`id`),
                           CONSTRAINT FK_userid2 FOREIGN KEY (userid) REFERENCES user(id),
                           UNIQUE KEY `userid` (`userid`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `t_admin` (

                           `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                           `userid` int(10) unsigned NOT NULL,
                           `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           PRIMARY KEY (`id`),
                           CONSTRAINT FK_userid3 FOREIGN KEY (userid) REFERENCES user(id),
                           UNIQUE KEY `userid` (`userid`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `formation` (

                           `formationTitle` varchar(25) NOT NULL,
                           `Level` varchar(255) NOT NULL,
                           `status` tinyint(1) DEFAULT 0,
                           `image` VARCHAR(255) NULL,
                           `desc` VARCHAR(255) NULL,
                           `time` int(5) NOT NULL ,
                           PRIMARY KEY (`formationTitle`)


) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `branche` (

                           `nom` varchar(255) NOT NULL,
                           `prepreq` varchar(255) DEFAULT NULL,
                           `formationTitle` varchar(25) NOT NULL,
                           `orderInt` int(5) NOT NULL,
                           CONSTRAINT FK_formationTitle FOREIGN KEY (formationTitle) REFERENCES formation(formationTitle),
                           PRIMARY KEY (`nom`)


) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `t_student` (

                           `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                           `userid` int(10) unsigned NOT NULL,
                           `fk_brancheName` varchar(255) NOT NULL,
                           `accepted` tinyint(1) DEFAULT 0,
                           `rejected` tinyint(1) DEFAULT 0,
                           `finished` tinyint(1) DEFAULT 0,
                           `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `removed` tinyint(1) DEFAULT 0,
                           CONSTRAINT FK_userid4 FOREIGN KEY (userid) REFERENCES user(id),
                           CONSTRAINT FK_brancheName3 FOREIGN KEY (fk_brancheName) REFERENCES branche(nom),
                           PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `t_teacher` (

                             `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                             `userid` int(10) unsigned NOT NULL,
                             `fk_brancheName` varchar(255) NOT NULL,
                             `accepted` tinyint(1) DEFAULT 0,
                             `rejected` tinyint(1) DEFAULT 0,
                             `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             CONSTRAINT FK_userid5 FOREIGN KEY (userid) REFERENCES user(id),
                             CONSTRAINT FK_brancheName2 FOREIGN KEY (fk_brancheName) REFERENCES branche(nom),
                             PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `course` (

                           `nom` varchar(255) NOT NULL,
                           `FK_branchename` varchar(255) NOT NULL,
                           `abandoned` tinyint(1) DEFAULT 0,
                           CONSTRAINT FK_branchename FOREIGN KEY (FK_branchename) REFERENCES branche(nom),
                           PRIMARY KEY (`nom`)


) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `yearly_course` (

                                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                  `fk_courseName` varchar(255) NOT NULL,
                                  `year` int(10) unsigned NOT NULL,
                                  `teacher` int(10) unsigned DEFAULT NULL,
                                  `status` tinyint(1) DEFAULT 0,
                                  CONSTRAINT FK_coursename4 FOREIGN KEY (fk_courseName) REFERENCES course(nom),
                                  PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `student_course` (

                             `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                             `fk_userid` int(10) unsigned NOT NULL,
                             `fk_yearly_courseID` int(10) unsigned NOT NULL,
                             `finished` tinyint(1) DEFAULT 0,
                             `FinishedDate` datetime  DEFAULT NULL ,
                             `note` int(10) DEFAULT NULL,
                             `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             CONSTRAINT FK_userid6 FOREIGN KEY (fk_userid) REFERENCES t_student(userid),
                             CONSTRAINT FK_courseid1 FOREIGN KEY (fk_yearly_courseID) REFERENCES yearly_course(id),
                             UNIQUE KEY `fk_userid` (`fk_userid`,`fk_yearly_courseID`),
                             PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `teacher_course` (

                                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                  `fk_userid` int(10) unsigned NOT NULL,
                                  `fk_yearly_courseID` int(10) unsigned NOT NULL,
                                  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  UNIQUE KEY `fk_userid` (`fk_userid`,`fk_yearly_courseID`),
                                  CONSTRAINT FK_userid7 FOREIGN KEY (fk_userid) REFERENCES t_teacher(userid),
                                  CONSTRAINT FK_courseid2 FOREIGN KEY (fk_yearly_courseID) REFERENCES yearly_course(id),
                                  PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `year` (

                                  `year` int(4) unsigned NOT NULL,
                                  `current`  tinyint(1) DEFAULT 1,
                                  PRIMARY KEY (`year`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;