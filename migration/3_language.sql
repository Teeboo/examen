CREATE TABLE IF NOT EXISTS `language`(
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    `languageId` VARCHAR(5) NOT NULL,

    PRIMARY KEY (`id`),
    UNIQUE KEY (`languageId`)

    )ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `language` (languageId,name) VALUES
    ('fr','Francais'),
    ('en','Anglais');

