CREATE TABLE `user` (
                        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                        `username` varchar(255) NOT NULL,
                        `password` varchar(255) NOT NULL,
                        `email` varchar(255) NOT NULL,
                        `lang` VARCHAR(5) NULL,
                        `firstname` varchar(255) NOT NULL,
                        `lastname` varchar(255) NOT NULL,
                        `countryid` varchar(10) NOT NULL,
                        `birthday` datetime NULL,
                        `address` varchar(255) NOT NULL,
                        `phone` int(30) NOT NULL,
                        `created` datetime NOT NULL,
                        `updated` datetime DEFAULT NULL,
                        `lastlogin` datetime DEFAULT NULL,
                        `admin` tinyint(1) DEFAULT NULL,
                        `image` VARCHAR(255) NULL,
                        `theme` VARCHAR(255) DEFAULT 'Classic',
                        `newsletter` tinyint  NOT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `email` (`email`),
                        UNIQUE KEY `username` (`username`)
                    )

                    ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO user (username,password,email,lang,firstname,lastname,countryid,address,phone,created,admin,theme,newsletter) VALUES
    ('Admin','$2y$10$YFz58hldHOKhlubuvTs7WOGH/APnSVJiKZvBF1csmvAUR1jQvDSoW','Admin@hotmail.com','fr','Admin','Admin','be','Admin village',111,now(),1,'Classic',0);