CREATE TABLE `migration` (
                             `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                             `lasttime` bigint(20) unsigned NOT NULL,
                             `filename` varchar(255) NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
                        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                        `username` varchar(255) NOT NULL,
                        `password` varchar(255) NOT NULL,
                        `email` varchar(255) NOT NULL,
                        `lang` VARCHAR(5) NULL,
                        `firstname` varchar(255) NOT NULL,
                        `lastname` varchar(255) NOT NULL,
                        `countryid` varchar(10) NOT NULL,
                        `birthday` datetime NULL,
                        `address` varchar(255) NOT NULL,
                        `phone` int(30) NOT NULL,
                        `created` datetime NOT NULL,
                        `updated` datetime DEFAULT NULL,
                        `lastlogin` datetime DEFAULT NULL,
                        `admin` tinyint(1) DEFAULT NULL,
                        `image` VARCHAR(255) NULL,
                        `theme` VARCHAR(255) DEFAULT 'Classic',
                        `newsletter` tinyint  NOT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `email` (`email`),
                        UNIQUE KEY `username` (`username`)
)

    ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `language`(
                                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                         `name` varchar(50) NOT NULL,
                                         `languageId` VARCHAR(5) NOT NULL,

                                         PRIMARY KEY (`id`),
                                         UNIQUE KEY (`languageId`)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `language` (languageId,name) VALUES
                                             ('fr','Francais'),
                                             ('en','Anglais');

CREATE TABLE IF NOT EXISTS `country`(
                                        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                        `countryname` varchar(50) NOT NULL,
                                        `countryId` VARCHAR(5) NOT NULL,

                                        PRIMARY KEY (`id`),
                                        UNIQUE KEY (`countryId`)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `country` (countryId, countryname) VALUES
                                                   ("be", 'Belgique'),
                                                   ("fr", 'France'),
                                                   ("al", 'Allemagne'),
                                                   ("ru", 'Russie'),
                                                   ("ot", 'Others');
CREATE TABLE `t_ban` (

                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `userid` int(10) unsigned NOT NULL,
                         `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         PRIMARY KEY (`id`),
                         CONSTRAINT FK_userid1 FOREIGN KEY (userid) REFERENCES user(id),
                         UNIQUE KEY `userid` (`userid`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `t_guest` (

                           `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                           `userid` int(10) unsigned NOT NULL,
                           `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           PRIMARY KEY (`id`),
                           CONSTRAINT FK_userid2 FOREIGN KEY (userid) REFERENCES user(id),
                           UNIQUE KEY `userid` (`userid`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `t_admin` (

                           `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                           `userid` int(10) unsigned NOT NULL,
                           `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           PRIMARY KEY (`id`),
                           CONSTRAINT FK_userid3 FOREIGN KEY (userid) REFERENCES user(id),
                           UNIQUE KEY `userid` (`userid`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `formation` (

                             `formationTitle` varchar(25) NOT NULL,
                             `Level` varchar(255) NOT NULL,
                             `status` tinyint(1) DEFAULT 0,
                             `image` VARCHAR(255) NULL,
                             `desc` VARCHAR(255) NULL,
                             `time` int(5) NOT NULL ,
                             PRIMARY KEY (`formationTitle`)


) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `branche` (

                           `nom` varchar(255) NOT NULL,
                           `prepreq` varchar(255) DEFAULT NULL,
                           `formationTitle` varchar(25) NOT NULL,
                           `orderInt` int(5) NOT NULL,
                           CONSTRAINT FK_formationTitle FOREIGN KEY (formationTitle) REFERENCES formation(formationTitle),
                           PRIMARY KEY (`nom`)


) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `t_student` (

                             `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                             `userid` int(10) unsigned NOT NULL,
                             `fk_brancheName` varchar(255) NOT NULL,
                             `accepted` tinyint(1) DEFAULT 0,
                             `rejected` tinyint(1) DEFAULT 0,
                             `finished` tinyint(1) DEFAULT 0,
                             `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `removed` tinyint(1) DEFAULT 0,
                             CONSTRAINT FK_userid4 FOREIGN KEY (userid) REFERENCES user(id),
                             CONSTRAINT FK_brancheName3 FOREIGN KEY (fk_brancheName) REFERENCES branche(nom),
                             PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `t_teacher` (

                             `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                             `userid` int(10) unsigned NOT NULL,
                             `fk_brancheName` varchar(255) NOT NULL,
                             `accepted` tinyint(1) DEFAULT 0,
                             `rejected` tinyint(1) DEFAULT 0,
                             `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             CONSTRAINT FK_userid5 FOREIGN KEY (userid) REFERENCES user(id),
                             CONSTRAINT FK_brancheName2 FOREIGN KEY (fk_brancheName) REFERENCES branche(nom),
                             PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `course` (

                          `nom` varchar(255) NOT NULL,
                          `FK_branchename` varchar(255) NOT NULL,
                          `abandoned` tinyint(1) DEFAULT 0,
                          CONSTRAINT FK_branchename FOREIGN KEY (FK_branchename) REFERENCES branche(nom),
                          PRIMARY KEY (`nom`)


) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `yearly_course` (

                                 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                 `fk_courseName` varchar(255) NOT NULL,
                                 `year` int(10) unsigned NOT NULL,
                                 `teacher` int(10) unsigned DEFAULT NULL,
                                 `status` tinyint(1) DEFAULT 0,
                                 CONSTRAINT FK_coursename4 FOREIGN KEY (fk_courseName) REFERENCES course(nom),
                                 PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `student_course` (

                                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                  `fk_userid` int(10) unsigned NOT NULL,
                                  `fk_yearly_courseID` int(10) unsigned NOT NULL,
                                  `finished` tinyint(1) DEFAULT 0,
                                  `FinishedDate` datetime  DEFAULT NULL ,
                                  `note` int(10) DEFAULT NULL,
                                  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  CONSTRAINT FK_userid6 FOREIGN KEY (fk_userid) REFERENCES t_student(userid),
                                  CONSTRAINT FK_courseid1 FOREIGN KEY (fk_yearly_courseID) REFERENCES yearly_course(id),
                                  UNIQUE KEY `fk_userid` (`fk_userid`,`fk_yearly_courseID`),
                                  PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `teacher_course` (

                                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                  `fk_userid` int(10) unsigned NOT NULL,
                                  `fk_yearly_courseID` int(10) unsigned NOT NULL,
                                  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  UNIQUE KEY `fk_userid` (`fk_userid`,`fk_yearly_courseID`),
                                  CONSTRAINT FK_userid7 FOREIGN KEY (fk_userid) REFERENCES t_teacher(userid),
                                  CONSTRAINT FK_courseid2 FOREIGN KEY (fk_yearly_courseID) REFERENCES yearly_course(id),
                                  PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `year` (

                        `year` int(4) unsigned NOT NULL,
                        `current`  tinyint(1) DEFAULT 1,
                        PRIMARY KEY (`year`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO formation () VALUES
                             ('Informatique de gestion','Bachelier',1,'image\\Course\\Bacinfo.jpg','Le bachelier en informatique de gestion est un collaborateur polyvalent qui met en oeuvre la diversité méthodologique des différentes fonctions de l’informaticien en réponse aux besoins des organisations.',3),
                             ('Webdeveloper','BES',1,'image\\Course\\WebDev.jpg','Le Web developer participe aux activités concernant la conception, la réalisation, la mise à jour, la maintenance et l''évaluation d''applications internet/intranet statiques et dynamiques.',2);

INSERT INTO branche (nom,formationTitle,prepreq,orderInt) VALUES
                                                              ('BacInfo1','Informatique de gestion',NULL,1),
                                                              ('BacInfo2','Informatique de gestion','BacInfo1',2),
                                                              ('BacInfo3','Informatique de gestion','BacInfo2',3),
                                                              ('BacInfo4','Informatique de gestion','BacInfo3',4),
                                                              ('Webdev01','Webdeveloper',NULL,1),
                                                              ('Webdev02','Webdeveloper','Webdev01',2);

INSERT INTO course (nom,FK_branchename) VALUES

                                            ('Initiation aux bases de données','BacInfo1'),
                                            ('Principes algorithmiques et programmation','BacInfo1'),
                                            ('Web : principes de base','BacInfo1'),
                                            ('Structure des ordinateurs','BacInfo1'),
                                            ('Gestion et exploitation de bases de données','BacInfo1'),
                                            ('Techniques de gestion de projet','BacInfo1'),
                                            ('Principes d’analyse informatique','BacInfo1'),

                                            ('Bases des réseaux','BacInfo2'),
                                            ('Projet de développement web','BacInfo2'),
                                            ('Programmation orientée objet','BacInfo2'),
                                            ('Projet d’analyse et de conception','BacInfo2'),
                                            ('Système d’exploitation','BacInfo2'),

                                            ('Administration, gestion et sécurisation des réseaux','BacInfo3'),
                                            ('Projet de développement SGBD','BacInfo3'),
                                            ('Notions de E-business','BacInfo3'),
                                            ('Veille technologique','BacInfo3'),

                                            ('Information et communication professionnelle','BacInfo4'),
                                            ('Produits logiciels de gestion intégrés','BacInfo4'),
                                            ('Mathématiques appliquées à l’informatique','BacInfo4'),


                                            ('Anglais UE1','Webdev01'),
                                            ('Création de sites web statiques','Webdev01'),
                                            ('Environnement et technologies du web','Webdev01'),
                                            ('Base des réseaux','Webdev01'),
                                            ('Approche Design','Webdev01'),
                                            ('Initiation à la programmation','Webdev01'),

                                            ('Anglais UE2','Webdev02'),
                                            ('SGBD - Système de gestion de bases de données','Webdev02'),
                                            ('Projet Web dynamique','Webdev02'),
                                            ('Scripts serveurs','Webdev02'),
                                            ('Scripts clients','Webdev02'),
                                            ('Framework et POO côté Serveur','Webdev02');

INSERT INTO yearly_course (fk_courseName, year) VALUES



                                                    ('Initiation aux bases de données',2022),
                                                    ('Principes algorithmiques et programmation',2022),
                                                    ('Web : principes de base',2022),
                                                    ('Structure des ordinateurs',2022),
                                                    ('Gestion et exploitation de bases de données',2022),
                                                    ('Techniques de gestion de projet',2022),
                                                    ('Principes d’analyse informatique',2022),

                                                    ('Bases des réseaux',2022),
                                                    ('Projet de développement web',2022),
                                                    ('Programmation orientée objet',2022),
                                                    ('Projet d’analyse et de conception',2022),
                                                    ('Système d’exploitation',2022),

                                                    ('Administration, gestion et sécurisation des réseaux',2022),
                                                    ('Projet de développement SGBD',2022),
                                                    ('Notions de E-business',2022),
                                                    ('Veille technologique',2022),

                                                    ('Information et communication professionnelle',2022),
                                                    ('Produits logiciels de gestion intégrés',2022),
                                                    ('Mathématiques appliquées à l’informatique',2022),


                                                    ('Anglais UE1',2022),
                                                    ('Création de sites web statiques',2022),
                                                    ('Environnement et technologies du web',2022),
                                                    ('Base des réseaux',2022),
                                                    ('Approche Design',2022),
                                                    ('Initiation à la programmation',2022),

                                                    ('Anglais UE2',2022),
                                                    ('SGBD - Système de gestion de bases de données',2022),
                                                    ('Projet Web dynamique',2022),
                                                    ('Scripts serveurs',2022),
                                                    ('Scripts clients',2022),
                                                    ('Framework et POO côté Serveur',2022);


INSERT INTO year VALUE (2022,1);

INSERT INTO user (username,password,email,lang,firstname,lastname,countryid,address,phone,created,admin,theme,newsletter) VALUES
    ('Admin','$2y$10$YFz58hldHOKhlubuvTs7WOGH/APnSVJiKZvBF1csmvAUR1jQvDSoW','Admin@hotmail.com','fr','Admin','Admin','be','Admin village',111,now(),1,'Classic',0);